#include "Ta/MutAutomaton.hpp"
#include "Ta/DotPrinter.hpp"
#include "Ta/Automaton.hpp"
#include "Ta/Signal.hpp"
#include "Ta/PropSignal.hpp"
#include "Ta/Runner.hpp"
#include "Ta/SignalBuilder.hpp"

#include <iostream>
#include <chrono>

using namespace Ta;

class BoolIter {
public:
	virtual bool hasNext() = 0;
	virtual bool moveNext() = 0;
};

class SquareWaveIter : public BoolIter {
public:
	SquareWaveIter(int negWidth, int posWidth, int phase0 = 0)
		: _negWidth(negWidth), _posWidth(posWidth), _phase0(phase0), _time(0) {}

	bool hasNext() { return true; }

	bool moveNext() {		
		int phase = (_time + _phase0) % (_negWidth + _posWidth);
		++_time;
		return phase >= _negWidth;
	}

private:
	int _negWidth;
	int _posWidth;
	int _phase0;
	int _time;
};

class SignalIter {
public:
	bool hasNext() { return true; }

	Signal moveNext(int count = 1) {		
		SignalBuilder builder(_time, _time + count, _iters.size());
		addNext(builder, count);
		return builder.build();
	}

	void addNext(SignalBuilder &builder, int count = 1) {
		for (int sampleI = 0; sampleI < count; ++sampleI) {
			for (size_t atomI = 0, size = _iters.size(); atomI < size; ++atomI) {
				builder[AtomRef(atomI)].add(_time + 1, _iters[atomI]->moveNext());
			}
			++_time;
		}
	}

private:
	SignalIter(std::vector<std::unique_ptr<BoolIter>> &&iters)
		: _time(0), _iters(std::move(iters)) {}

	int _time;
	std::vector<std::unique_ptr<BoolIter>> _iters;

	friend class SignalIterBuilder;
};

class SignalIterBuilder {
public:
	SignalIterBuilder(size_t size) : _iters(size) {}

	template<typename TIter, typename... TArg>
	void set(AtomRef atom, TArg&&... iterArgs) {
		if (atom.id() >= _iters.size()) {
			throw std::out_of_range("Atom id is outside of the builder's range.");
		}
		_iters[atom.id()] = std::make_unique<TIter>(std::forward<TArg>(iterArgs)...);
	}

	SignalIter build() {
		for (size_t i = 0, size = _iters.size(); i < size; ++i) {
			if (!_iters[i])
				throw std::runtime_error("Iterator for every component should be assigned.");			
		}
		return SignalIter(std::move(_iters));
	}

private:
	std::vector<std::unique_ptr<BoolIter>> _iters;
};

Automaton makeB0() {
	AtomRef b0(0);
	Clock matchDuration(0);

	MutAutomaton mutA;	

	MutState &s0 = mutA.makeState();

	MutState &s1 = mutA.makeState();
	s1._inputInv = PropRef(b0);
	s1._acceptCond = Zone(ZONE_TOP);

	mutA.makeInit(s0);
	MutTrans &t01 = mutA.makeTrans(s0, s1);
	t01._commands = CmdList(Cmd(CMD_RESET, matchDuration));

	return mutA.build();
}

Automaton makeB0B1(int maxSegmentDuration = -1) {
	AtomRef b0(0), b1(1);
	Clock matchDuration(0);
	Clock c(1);

	MutAutomaton mutA;
	
	MutState &s0 = mutA.makeState();
	mutA.makeInit(s0);

	MutState &s1 = mutA.makeState();
	s1._inputInv = PropRef(b0);
	if (maxSegmentDuration >= 0) {
		s1._clockInv = Zone{ Constr(c, Clock(), Cmp::LEQ, maxSegmentDuration) };
	}

	MutTrans &t01 = mutA.makeTrans(s0, s1);
	if (maxSegmentDuration >= 0) {
		t01._commands = CmdList{ Cmd(CMD_RESET, matchDuration), Cmd(CMD_RESET, c) };
	}
	else {
		t01._commands = CmdList{ Cmd(CMD_RESET, matchDuration) };
	}

	MutState &s2 = mutA.makeState();
	s2._inputInv = PropRef(b1);
	if (maxSegmentDuration >= 0) {
		s2._clockInv = Zone{ Constr(c, Clock(), Cmp::LEQ, maxSegmentDuration) };
		s2._acceptCond = Zone{ Constr(c, Clock(), Cmp::LEQ, maxSegmentDuration) };
	}
	else {
		s2._acceptCond = Zone(ZONE_TOP);
	}

	MutTrans &t12 = mutA.makeTrans(s1, s2);
	if (maxSegmentDuration >= 0) {
		t12._commands = CmdList(Cmd(CMD_RESET, c));
	}

	return mutA.build();
}

Automaton makeB0B1B2(int maxSegmentDuration = -1) {
	AtomRef b0(0), b1(1), b2(2);
	Clock matchDuration(0);
	Clock c(1);

	MutAutomaton mutA;

	MutState &s0 = mutA.makeState();
	mutA.makeInit(s0);

	MutState &s1 = mutA.makeState();
	s1._inputInv = PropRef(b0);
	if (maxSegmentDuration >= 0) {
		s1._clockInv = Zone{ Constr(c, Clock(), Cmp::LEQ, maxSegmentDuration) };
	}

	MutTrans &t01 = mutA.makeTrans(s0, s1);
	t01._commands = CmdList{ Cmd(CMD_RESET, matchDuration), Cmd(CMD_RESET, c) };

	MutState &s2 = mutA.makeState();
	s2._inputInv = PropRef(b1);
	if (maxSegmentDuration >= 0) {
		s2._clockInv = Zone{ Constr(c, Clock(), Cmp::LEQ, maxSegmentDuration) };
	}

	MutTrans &t12 = mutA.makeTrans(s1, s2);
	t12._commands = CmdList(Cmd(CMD_RESET, c));
	
	MutState &s3 = mutA.makeState();
	s3._inputInv = PropRef(b2);
	if (maxSegmentDuration >= 0) {
		s3._clockInv = Zone{ Constr(c, Clock(), Cmp::LEQ, maxSegmentDuration) };
		s3._acceptCond = Zone{ Constr(c, Clock(), Cmp::LEQ, maxSegmentDuration) };
	}
	else {
		s3._acceptCond = Zone(ZONE_TOP);
	}

	MutTrans &t23 = mutA.makeTrans(s2, s3);
	t23._commands = CmdList(Cmd(CMD_RESET, c));	

	return mutA.build();
}

Automaton makeB0orB1dotB2orB3() {	
	AtomRef b0(0), b1(1), b2(2), b3(3);
	Clock matchDuration(0);

	MutAutomaton mutA;

	MutState &s0 = mutA.makeState();
	mutA.makeInit(s0);

	MutState &s1 = mutA.makeState();
	s1._inputInv = PropRef(b0);

	MutTrans &t01 = mutA.makeTrans(s0, s1);
	t01._commands = CmdList{ Cmd(CMD_RESET, matchDuration) };

	MutState &s2 = mutA.makeState();
	s2._inputInv = PropRef(b1);

	MutTrans &t02 = mutA.makeTrans(s0, s2);
	t02._commands = CmdList{ Cmd(CMD_RESET, matchDuration) };

	MutState &s3 = mutA.makeState();
	s3._inputInv = PropRef(b2);
	s3._acceptCond = Zone(ZONE_TOP);
	mutA.makeTrans(s1, s3);
	mutA.makeTrans(s2, s3);

	MutState &s4 = mutA.makeState();
	s4._inputInv = PropRef(b3);
	s4._acceptCond = Zone(ZONE_TOP);
	mutA.makeTrans(s1, s4);
	mutA.makeTrans(s2, s4);

	return mutA.build();
}

Automaton makeB1dotB0B2Star(int maxDuration = -1) {
	AtomRef b0(0), b1(1), b2(2);
	Clock matchDuration(0);
	Clock c(1);

	MutAutomaton mutA;

	MutState &s0 = mutA.makeState();
	mutA.makeInit(s0);

	MutState &s1 = mutA.makeState();
	s1._inputInv = PropRef(b0);
	if (maxDuration >= 0) {
		s1._clockInv = Zone{ Constr(c, Clock(), Cmp::LEQ, maxDuration) };
		s1._acceptCond = Zone{ Constr(c, Clock(), Cmp::LEQ, maxDuration) };
	}
	else {
		s1._acceptCond = Zone(ZONE_TOP);
	}

	MutTrans &t01 = mutA.makeTrans(s0, s1);
	t01._commands = CmdList{ Cmd(CMD_RESET, matchDuration), Cmd(CMD_RESET, c) };

	MutState &s2 = mutA.makeState();
	s2._inputInv = PropRef(b1);
	if (maxDuration >= 0) {
		s2._clockInv = Zone{ Constr(c, Clock(), Cmp::LEQ, maxDuration) };
	}

	mutA.makeTrans(s1, s2);

	MutState &s3 = mutA.makeState();
	s3._inputInv = PropRef(b2);
	if (maxDuration >= 0) {
		s3._clockInv = Zone{ Constr(c, Clock(), Cmp::LEQ, maxDuration) };
		s3._acceptCond = Zone{ Constr(c, Clock(), Cmp::LEQ, maxDuration) };
	}
	else {
		s3._acceptCond = Zone(ZONE_TOP);
	}

	mutA.makeTrans(s2, s3);
	mutA.makeTrans(s3, s2);	

	return mutA.build();
}

Automaton makeRedundancyAutomaton(PropSet &propSet) {
	AtomRef b0(0), b1(1);
	PropRef b0NotB1 = propSet.makeAnd({ PropRef(b0), propSet.makeNot(PropRef(b1)) });
	PropRef b0B1 = propSet.makeAnd({ PropRef(b0), PropRef(b1) });
	PropRef notB0B1 = propSet.makeAnd({ propSet.makeNot(PropRef(b0)), PropRef(b1) });

	Clock clockMatch(0);
	Clock clockB0(1);
	Clock clockB0B1(2);
	Clock clockB1(3);

	MutAutomaton mutA;

	MutState &s0 = mutA.makeState();
	mutA.makeInit(s0);

	MutState &s1 = mutA.makeState();
	s1._inputInv = b0NotB1;	
	s1._clockInv = Zone{ Constr(clockB0, Clock(), Cmp::LEQ, 5) };

	MutTrans &t01 = mutA.makeTrans(s0, s1);
	t01._commands = CmdList{ Cmd(CMD_RESET, clockMatch), Cmd(CMD_RESET, clockB0) };	

	MutState &s2 = mutA.makeState();
	s2._inputInv = b0B1;
	s2._clockInv = Zone{
		Constr(clockB0, Clock(), Cmp::LEQ, 5),
		Constr(clockB0B1, Clock(), Cmp::LEQ, 2),
		Constr(clockB1, Clock(), Cmp::LEQ, 5) };

	MutTrans &t12 = mutA.makeTrans(s1, s2);
	t12._commands = CmdList({ Cmd(CMD_RESET, clockB0B1), Cmd(CMD_RESET, clockB1) });

	MutState &s3 = mutA.makeState();
	s3._inputInv = notB0B1;
	s3._clockInv = Zone{ Constr(clockB1, Clock(), Cmp::LEQ, 5) };
	s3._acceptCond = Zone{ Constr(clockB1, Clock(), Cmp::LEQ, 5), Constr(Clock(), clockB1, Cmp::LEQ, -4), };
	
	MutTrans &t23 = mutA.makeTrans(s2, s3);
	t23._guard = Zone{
		Constr(clockB0, Clock(), Cmp::LEQ, 5), Constr(Clock(), clockB0, Cmp::LEQ, -4),
		Constr(clockB0B1, Clock(), Cmp::LEQ, 2), Constr(Clock(), clockB0B1, Cmp::LEQ, -1)
	};

	return mutA.build();
}

Automaton makeJitterWave200(PropSet &propSet) {
	AtomRef b0(0);
	PropRef notB0 = propSet.makeNot(PropRef(b0));

	MutAutomaton mutA;

	Clock c(0);

	MutState &s0 = mutA.makeState();
	s0._inputInv = notB0;
	s0._clockInv = Zone{ Constr(c, Clock(), Cmp::LEQ, 100) };

	mutA.makeInit(s0, Cmd(CMD_RESET, c));

	MutState &s1 = mutA.makeState();
	s1._clockInv = Zone{ Constr(c, Clock(), Cmp::LEQ, 100) };
	s1._allowTime = false;

	mutA.makeTrans(s0, s1);

	MutState &s2 = mutA.makeState();
	s2._clockInv = Zone{ Constr(c, Clock(), Cmp::LEQ, 1) };
	s2._allowTime = false;

	MutTrans &t12 = mutA.makeTrans(s1, s2);
	t12._guard = Zone{ Constr(c, Clock(), Cmp::LEQ, 100), Constr(Clock(), c, Cmp::LEQ, -98) };
	t12._commands = CmdList(Cmd(CMD_RESET, c));

	MutState &s3 = mutA.makeState();
	s3._inputInv = PropRef(b0);
	s3._clockInv = Zone{ Constr(c, Clock(), Cmp::LEQ, 1) };

	mutA.makeTrans(s2, s3);

	MutState &s4 = mutA.makeState();
	s4._inputInv = PropRef(b0);
	s4._clockInv = Zone{ Constr(c, Clock(), Cmp::LEQ, 100) };

	MutTrans &t34 = mutA.makeTrans(s3, s4);
	t34._guard = Zone{ Constr(c, Clock(), Cmp::LEQ, 1), Constr(Clock(), c, Cmp::LEQ, -1) };
	t34._commands = CmdList(Cmd(CMD_RESET, c));

	MutState &s5 = mutA.makeState();
	s5._clockInv = Zone{ Constr(c, Clock(), Cmp::LEQ, 100) };
	s5._allowTime = false;

	mutA.makeTrans(s4, s5);

	MutState &s6 = mutA.makeState();
	s6._clockInv = Zone{ Constr(c, Clock(), Cmp::LEQ, 1) };
	s6._allowTime = false;
	s6._acceptCond = Zone(ZONE_TOP);

	MutTrans &t56 = mutA.makeTrans(s5, s6);
	t56._guard = Zone{ Constr(c, Clock(), Cmp::LEQ, 100), Constr(Clock(), c, Cmp::LEQ, -98) };
	t56._commands = CmdList(Cmd(CMD_RESET, c));

	MutState &s7 = mutA.makeState();
	s3._inputInv = notB0;
	s3._clockInv = Zone{ Constr(c, Clock(), Cmp::LEQ, 1) };

	mutA.makeTrans(s6, s7);

	MutTrans &t70 = mutA.makeTrans(s7, s0);
	t70._guard = Zone{ Constr(c, Clock(), Cmp::LEQ, 1), Constr(Clock(), c, Cmp::LEQ, -1) };
	t70._commands = CmdList(Cmd(CMD_RESET, c));

	return mutA.build();
}

SignalIter makeWave2() {
	AtomRef b0(0), b1(1);
	SignalIterBuilder builder(2);
	builder.set<SquareWaveIter>(b0, 1, 1);
	builder.set<SquareWaveIter>(b1, 1, 1, 1);
	return builder.build();
}

SignalIter makeWave200() {
	AtomRef b0(0), b1(1), b2(2), b3(3);
	SignalIterBuilder builder(4);
	builder.set<SquareWaveIter>(b0, 100, 100);
	builder.set<SquareWaveIter>(b1, 100, 100, 150);
	builder.set<SquareWaveIter>(b2, 100, 100, 300);
	builder.set<SquareWaveIter>(b3, 100, 100, 450);
	return builder.build();
}

SignalIter makeRedundancyWave() {
	AtomRef b0(0), b1(1);
	SignalIterBuilder builder(2);
	builder.set<SquareWaveIter>(b0, 10, 5);
	builder.set<SquareWaveIter>(b1, 28, 4);
	return builder.build();
}

SignalIter makeExampleWave() {
	AtomRef b0(0), b1(1);
	SignalIterBuilder builder(2);
	builder.set<SquareWaveIter>(b0, 2, 2, 1);
	builder.set<SquareWaveIter>(b1, 4, 4, 2);
	return builder.build();
}

void run(PropSet &propSet, const Automaton &automaton, SignalIter &signalIter, int duration, int step, const char *name = nullptr, bool printDot = false, bool printLog = true) {
	if (printLog) {
		if (name) {
			std::cerr << name << "\n";
		}
		std::cerr << duration << " samples in blocks of " << step << "\n";
	}
	if (printDot) {
		std::cout << toDot(automaton);
	}
	auto startWallTime = std::chrono::high_resolution_clock::now();
	// A scope to make sure that Runner's destructor runtime is included.
	// At this point we don't include the potentially couple of seconds of IF's static desctructor, but that's ok.
	{
		// TODO: Implement finding first unused clock for an automaton.
		Clock time(99);
		Runner r(automaton, time);
		int lastPercent = 0;
		const int PERCENT_STEP = 10;
		size_t acceptCount = 0;
		for (int i = 0; i < duration; i += step) {
			Signal signal(signalIter.moveNext(step));
			PropSignal propSignal(signal, propSet);
			r.run(propSignal);
			acceptCount += r.acceptConfigCount();
			if (printLog) {
				int newPercent = (100 * (i + step)) / duration;
				if (newPercent >= lastPercent + PERCENT_STEP) {
					lastPercent = newPercent;
					std::cerr << newPercent << "% ";
				}
			}
			if (printDot) {
				std::cout << toDot(r) << "\n";
			}
		}
		if (printLog) {
			std::cerr << "\n";
			std::cerr << "Configurations: " << r.totalConfigCount() - acceptCount << "\n";
			std::cerr << "Accepting configurations: " << acceptCount << "\n";
		}
	}
	std::chrono::duration<double> elapsedWallTime = std::chrono::high_resolution_clock::now() - startWallTime;
	if (printLog) {
		std::cerr << "Elapsed: " << elapsedWallTime.count() << "s\n";		
	}

	
}

int main(int argc, char **argv) {
	PropSet propSet;

	// run(propSet, makeB0B1(), makeExampleWave(), 8, 4, "Example", true);

	// run(propSet, makeB0(), makeWave2(), 10000, 10000, "b0, wave 2");
	// run(propSet, makeB0(), makeWave2(), 100000, 100000, "b0, wave 2");
	// run(propSet, makeB0(), makeWave2(), 1000000, 1000000, "b0, wave 2");
	// run(propSet, makeB0(), makeWave2(), 10000, 200, "b0, wave 2");
	// run(propSet, makeB0(), makeWave2(), 100000, 200, "b0, wave 2");
	// run(propSet, makeB0(), makeWave2(), 1000000, 200, "b0, wave 2");
	// run(propSet, makeB0(), makeWave2(), 10000, 1, "b0, wave 2");
	// run(propSet, makeB0(), makeWave2(), 100000, 1, "b0, wave 2");
	// run(propSet, makeB0(), makeWave2(), 1000000, 1, "b0, wave 2");

	// run(propSet, makeB0B1(), makeWave2(), 10000, 10000, "b0.b1, wave 2");
	// run(propSet, makeB0B1(), makeWave2(), 100000, 100000, "b0.b1, wave 2");
	// run(propSet, makeB0B1(), makeWave2(), 1000000, 1000000, "b0.b1, wave 2");
	// run(propSet, makeB0B1(), makeWave2(), 10000, 200, "b0.b1, wave 2");
	// run(propSet, makeB0B1(), makeWave2(), 100000, 200, "b0.b1, wave 2");
	// run(propSet, makeB0B1(), makeWave2(), 1000000, 200, "b0.b1, wave 2");
	// run(propSet, makeB0B1(), makeWave2(), 10000, 1, "b0.b1, wave 2");
	// run(propSet, makeB0B1(), makeWave2(), 100000, 1, "b0.b1, wave 2");
	// run(propSet, makeB0B1(), makeWave2(), 1000000, 1, "b0.b1, wave 2");

	// run(propSet, makeB0B1(), makeWave200(), 10000, 10000, "b0.b1, wave 200");
	// run(propSet, makeB0B1(), makeWave200(), 100000, 100000, "b0.b1, wave 200");
	// run(propSet, makeB0B1(), makeWave200(), 1000000, 1000000, "b0.b1, wave 200");
	// run(propSet, makeB0B1(), makeWave200(), 10000, 200, "b0.b1, wave 200");
	// run(propSet, makeB0B1(), makeWave200(), 100000, 200, "b0.b1, wave 200");
	// run(propSet, makeB0B1(), makeWave200(), 1000000, 200, "b0.b1, wave 200");
	// run(propSet, makeB0B1(), makeWave200(), 10000, 1, "b0.b1, wave 200");
	// run(propSet, makeB0B1(), makeWave200(), 100000, 1, "b0.b1, wave 200");
	// run(propSet, makeB0B1(), makeWave200(), 1000000, 1, "b0.b1, wave 200");

	// run(propSet, makeB0B1(20), makeWave200(), 10000, 10000, "<b0>_[0,20].<b1>_[0,20], wave 200");
	// run(propSet, makeB0B1(20), makeWave200(), 100000, 100000, "<b0>_[0,20].<b1>_[0,20], wave 200");
	// run(propSet, makeB0B1(20), makeWave200(), 1000000, 1000000, "<b0>_[0,20].<b1>_[0,20], wave 200");
	// run(propSet, makeB0B1(20), makeWave200(), 10000, 200, "<b0>_[0,20].<b1>_[0,20], wave 200");
	// run(propSet, makeB0B1(20), makeWave200(), 100000, 200, "<b0>_[0,20].<b1>_[0,20], wave 200");
	// run(propSet, makeB0B1(20), makeWave200(), 1000000, 200, "<b0>_[0,20].<b1>_[0,20], wave 200");
	run(propSet, makeB0B1(20), makeWave200(), 10000, 1, "<b0>_[0,20].<b1>_[0,20], wave 200");
	// run(propSet, makeB0B1(20), makeWave200(), 100000, 1, "<b0>_[0,20].<b1>_[0,20], wave 200");
	// run(propSet, makeB0B1(20), makeWave200(), 1000000, 1, "<b0>_[0,20].<b1>_[0,20], wave 200");

	// run(propSet, makeB0B1B2(), makeWave200(), 10000, 10000, "b0.b1.b2, wave 200");
	// run(propSet, makeB0B1B2(), makeWave200(), 100000, 100000, "b0.b1.b2, wave 200");
	// run(propSet, makeB0B1B2(), makeWave200(), 1000000, 1000000, "b0.b1.b2, wave 200");
	// run(propSet, makeB0B1B2(), makeWave200(), 10000, 200, "b0.b1.b2, wave 200");
	// run(propSet, makeB0B1B2(), makeWave200(), 100000, 200, "b0.b1.b2, wave 200");
	// run(propSet, makeB0B1B2(), makeWave200(), 1000000, 200, "b0.b1.b2, wave 200");
	// run(propSet, makeB0B1B2(), makeWave200(), 10000, 1, "b0.b1.b2, wave 200");
	// run(propSet, makeB0B1B2(), makeWave200(), 100000, 1, "b0.b1.b2, wave 200");
	// run(propSet, makeB0B1B2(), makeWave200(), 1000000, 1, "b0.b1.b2, wave 200");

	// run(propSet, makeB0B1B2(20), makeWave200(), 10000, 10000, "<b0>_[0,20].<b1>_[0,20].<b2>_[0,20], wave 200");
	// run(propSet, makeB0B1B2(20), makeWave200(), 100000, 100000, "<b0>_[0,20].<b1>_[0,20].<b2>_[0,20], wave 200");
	// run(propSet, makeB0B1B2(20), makeWave200(), 1000000, 1000000, "<b0>_[0,20].<b1>_[0,20].<b2>_[0,20], wave 200");
	// run(propSet, makeB0B1B2(20), makeWave200(), 10000, 200, "<b0>_[0,20].<b1>_[0,20].<b2>_[0,20], wave 200");
	// run(propSet, makeB0B1B2(20), makeWave200(), 100000, 200, "<b0>_[0,20].<b1>_[0,20].<b2>_[0,20], wave 200");
	// run(propSet, makeB0B1B2(20), makeWave200(), 1000000, 200, "<b0>_[0,20].<b1>_[0,20].<b2>_[0,20], wave 200");
	// run(propSet, makeB0B1B2(20), makeWave200(), 10000, 1, "<b0>_[0,20].<b1>_[0,20].<b2>_[0,20], wave 200");
	// run(propSet, makeB0B1B2(20), makeWave200(), 100000, 1, "<b0>_[0,20].<b1>_[0,20].<b2>_[0,20], wave 200");
	// run(propSet, makeB0B1B2(20), makeWave200(), 1000000, 1, "<b0>_[0,20].<b1>_[0,20].<b2>_[0,20], wave 200");

	// run(propSet, makeB0orB1dotB2orB3(), makeWave200(), 10000, 10000, "(b0 | b1).(b2 | b3), wave 200");
	// run(propSet, makeB0orB1dotB2orB3(), makeWave200(), 100000, 100000, "(b0 | b1).(b2 | b3), wave 2002");
	// run(propSet, makeB0orB1dotB2orB3(), makeWave200(), 1000000, 1000000, "(b0 | b1).(b2 | b3), wave 200");
	// run(propSet, makeB0orB1dotB2orB3(), makeWave200(), 10000, 200, "(b0 | b1).(b2 | b3), wave 200");
	// run(propSet, makeB0orB1dotB2orB3(), makeWave200(), 100000, 200, "(b0 | b1).(b2 | b3), wave 200");
	// run(propSet, makeB0orB1dotB2orB3(), makeWave200(), 1000000, 200, "(b0 | b1).(b2 | b3), wave 200");
	// run(propSet, makeB0orB1dotB2orB3(), makeWave200(), 10000, 1, "(b0 | b1).(b2 | b3), wave 200");
	// run(propSet, makeB0orB1dotB2orB3(), makeWave200(), 100000, 1, "(b0 | b1).(b2 | b3), wave 200");
	// run(propSet, makeB0orB1dotB2orB3(), makeWave200(), 1000000, 1, "(b0 | b1).(b2 | b3), wave 200");

	// run(propSet, makeB1dotB0B2Star(), makeWave200(), 10000, 10000, "b1.(b0.b2)*, wave 200");
	// run(propSet, makeB1dotB0B2Star(), makeWave200(), 100000, 100000, "b1.(b0.b2)*, wave 200");
	// run(propSet, makeB1dotB0B2Star(), makeWave200(), 1000000, 1000000, "b1.(b0.b2)*, wave 200");
	// run(propSet, makeB1dotB0B2Star(), makeWave200(), 10000, 200, "b1.(b0.b2)*, wave 200");
	// run(propSet, makeB1dotB0B2Star(), makeWave200(), 100000, 200, "b1.(b0.b2)*, wave 200");
	// run(propSet, makeB1dotB0B2Star(), makeWave200(), 1000000, 200, "b1.(b0.b2)*, wave 200");
	// run(propSet, makeB1dotB0B2Star(), makeWave200(), 10000, 1, "b1.(b0.b2)*, wave 200");
	// run(propSet, makeB1dotB0B2Star(), makeWave200(), 100000, 1, "b1.(b0.b2)*, wave 200");
	// run(propSet, makeB1dotB0B2Star(), makeWave200(), 1000000, 1, "b1.(b0.b2)*, wave 200");

	// run(propSet, makeB1dotB0B2Star(1000), makeWave200(), 10000, 10000, "<b1.(b0.b2)*>_[0,1000], wave 200");
	// run(propSet, makeB1dotB0B2Star(1000), makeWave200(), 100000, 100000, "<b1.(b0.b2)*>_[0,1000], wave 200");
	// run(propSet, makeB1dotB0B2Star(1000), makeWave200(), 1000000, 1000000, "<b1.(b0.b2)*>_[0,1000], wave 200");
	// run(propSet, makeB1dotB0B2Star(1000), makeWave200(), 10000, 200, "<b1.(b0.b2)*>_[0,1000], wave 200");
	// run(propSet, makeB1dotB0B2Star(1000), makeWave200(), 100000, 200, "<b1.(b0.b2)*>_[0,1000], wave 200");
	// run(propSet, makeB1dotB0B2Star(1000), makeWave200(), 1000000, 200, "<b1.(b0.b2)*>_[0,1000], wave 200");
	// run(propSet, makeB1dotB0B2Star(1000), makeWave200(), 10000, 1, "<b1.(b0.b2)*>_[0,1000], wave 200");
	// run(propSet, makeB1dotB0B2Star(1000), makeWave200(), 100000, 1, "<b1.(b0.b2)*>_[0,1000], wave 200");
	// run(propSet, makeB1dotB0B2Star(1000), makeWave200(), 1000000, 1, "<b1.(b0.b2)*>_[0,1000], wave 200");

	// run(propSet, makeRedundancyAutomaton(propSet), makeRedundancyWave(), 10000, 10000, "Redundancy");
	// run(propSet, makeRedundancyAutomaton(propSet), makeRedundancyWave(), 100000, 100000, "Redundancy");
	// run(propSet, makeRedundancyAutomaton(propSet), makeRedundancyWave(), 1000000, 1000000, "Redundancy");
	// run(propSet, makeRedundancyAutomaton(propSet), makeRedundancyWave(), 10000, 30, "Redundancy");
	// run(propSet, makeRedundancyAutomaton(propSet), makeRedundancyWave(), 100000, 30, "Redundancy");
	// run(propSet, makeRedundancyAutomaton(propSet), makeRedundancyWave(), 1000000, 30, "Redundancy");
	// run(propSet, makeRedundancyAutomaton(propSet), makeRedundancyWave(), 10000, 1, "Redundancy");
	// run(propSet, makeRedundancyAutomaton(propSet), makeRedundancyWave(), 100000, 1, "Redundancy");
	// run(propSet, makeRedundancyAutomaton(propSet), makeRedundancyWave(), 1000000, 1, "Redundancy");

	// run(propSet, makeJitterWave200(propSet), makeWave200(), 10000, 10000, "Jitter wave");
	// run(propSet, makeJitterWave200(propSet), makeWave200(), 100000, 100000, "Jitter wave");
	// run(propSet, makeJitterWave200(propSet), makeWave200(), 1000000, 1000000, "Jitter wave");
	// TODO: Observe the partial order problem in this example.
	// run(propSet, makeJitterWave200(propSet), makeWave200(), 10000, 200, "Jitter wave");
	// run(propSet, makeJitterWave200(propSet), makeWave200(), 100000, 200, "Jitter wave");
	// run(propSet, makeJitterWave200(propSet), makeWave200(), 1000000, 200, "Jitter wave");
	// run(propSet, makeJitterWave200(propSet), makeWave200(), 10000, 1, "Jitter wave");
	// run(propSet, makeJitterWave200(propSet), makeWave200(), 100000, 1, "Jitter wave");
	// run(propSet, makeJitterWave200(propSet), makeWave200(), 1000000, 1, "Jitter wave");
}