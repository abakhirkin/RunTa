#include "Zone.hpp"

#include <stdexcept>

namespace Ta {

	IfInit IfZone::_init;

    std::ostream &operator<<(std::ostream & os, const IfZonePrinter &printer) {
        // TODO: Try to remove some redundant constraints when printing.
        if_type_zone zone = printer.zone().zone();
        if (!zone)
            return os << "false";
        
        bool firstLine = true;
        bool isTop = true;
        unsigned int dim = zone[0][0] + 1;
        for (size_t i = 1; i < dim; ++i) {
            for (size_t j = 0; j < i; ++j) {                
                bool hasRight = zone[i][j] < IF_BOUND_INFTY;
                bool hasLeft = zone[j][i] < IF_BOUND_INFTY;
                int rightBound = if_value_bound(zone[i][j]);
                int leftBound = -if_value_bound(zone[j][i]);
                if (hasRight || hasLeft) {
                    isTop = false;
                    if (firstLine)
                        firstLine = false;                        
                    else
                        os << printer.newline();                        
                    if (hasRight && hasLeft && rightBound != leftBound) {
                        os << leftBound << " " << (if_sign_bound(zone[j][i]) ? "<=" : "<") << " c" << zone[i][i] - 1;
                        if (j != 0)
                            os << " - c" << zone[j][j] - 1;
                        os << " " << (if_sign_bound(zone[i][j]) ? "<=" : "<") << " " << rightBound;
                    }
                    else {
                        os << "c" << zone[i][i] - 1;
                        if (j != 0)
                            os << " - c" << zone[j][j] - 1;
                        if (hasRight & !hasLeft)
                            os << " " << (if_sign_bound(zone[i][j]) ? "<=" : "<") << " " << rightBound;
                        else if (hasLeft && !hasRight)
                            os << " " << (if_sign_bound(zone[j][i]) ? ">=" : ">") << " " << leftBound;
                        else /*if (hasRight && hasLeft && rightBound == leftBound)*/
                            os << " = " << rightBound;
                    }                    
                }                
            }
        }

        if (isTop)
            os << "true";
        return os;
    }

    std::ostream & operator<<(std::ostream &os, const IfZone &zone) {
        return os << IfZonePrinter(zone);
    }
    
    void IfZone::exec(Cmd cmd) {
        switch (cmd.op()) {
        case Op::ASSIGN:
            assign(cmd.dest(), cmd.src(), cmd.value());
            break;
        case Op::FORGET:
            forget(cmd.dest());
            break;
        default:
            throw std::invalid_argument("Unknown command opcode.");
        }
    }

    void IfZone::exec(const CmdList &commands) {
        for (size_t i = 0, size = commands.size(); i < size; ++i) {
            exec(commands[i]);
        }
    }
}
