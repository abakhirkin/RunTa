#pragma once

#include <stdexcept>
#include <ostream>

namespace Ta {
    /*! \brief Comparison operator. */
    enum class Cmp {
        /*! \brief Strictly less. */
        LT = 0,
        /*! \brief Less or equal. */
        LEQ = 1
    };

    /*! \brief Evaluates the comparison. */
    template<typename T>
    bool cmpEval(Cmp cmp, const T &val1, const T &val2) {
        switch (cmp) {
        case Cmp::LT:
            return val1 < val2;
        case Cmp::LEQ:
            return val1 <= val2;
        default:
            throw std::runtime_error("Invalid comparison type.");
        }
    }

    /*! \brief Prints the comparison operator to the stream. */
    std::ostream &operator<<(std::ostream &os, Cmp cmp);
}
