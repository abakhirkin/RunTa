#pragma once

#include "Cmd.hpp"
#include "PrinterBase.hpp"

#include <vector>
#include <ostream>

namespace Ta {

    /*! \brief List of commands. */
    class CmdList {
    public:
        /*! \brief Creates an empty list of commands. */
        CmdList() : _commands() {}

        /*! \brief Creates a list with the single comand. */
        CmdList(Cmd cmd) : _commands(1, cmd) {}

        /*! \breif Creates a list with the givev commands. */
        CmdList(std::initializer_list<Cmd> commands) : _commands(commands) {}

        /*! \brief Number of commands. */
        size_t size() const {
            return _commands.size();
        }

        /*! \brief I-th command. */
        const Cmd &operator[](size_t i) const {
            return _commands[i];
        }

        bool updatesClock(Clock c) const;

    private:
        std::vector<Cmd> _commands;
    };

    /*! \brief Helper to print a command list to a stream.
    
        An instance of CmdListPrinter stores a reference to a command list and printing settings.
        Printing a CmdListPrinter to a stream with operator<< prints the referred command list with the
        given settings.
        
        Currently the only setting is the separator that gets printed between commands ("\n" by default). */
    class CmdListPrinter : public PrinterBase {
    public:
        /*! \brief Creates a new helper instance that stores a reference to the given command list. */
        CmdListPrinter(const CmdList &commands) : _commands(commands) {}

        /*! \brief Sets is the separator that gets printed between commands. */
        CmdListPrinter &setNewline(const char *newline) {
            PrinterBase::setNewline(newline);
            return *this;
        }

        /*! \brief Associated command list. */
        const CmdList &commands() const {
            return _commands;
        }

    private:
        const CmdList &_commands;
    };

    /*! \brief Prints the command list to the stream.
    
        \relates CmdListPrinter */
    std::ostream &operator<<(std::ostream &os, const CmdListPrinter &printer);

    /*! Prints the command list to the stream.
        
        \relates CmdList */
    std::ostream &operator<<(std::ostream &os, const CmdList &commands);    
}
