#include "SignalBuilder.hpp"

namespace Ta {
    
    SignalBuilder::SignalBuilder(int startTime, int endTime, size_t componentCount)
		: _startTime(startTime), _endTime(endTime) {
        for (size_t i = 0; i < componentCount; ++i) {
            _components.emplace_back(_startTime);
        }
    }

    Signal SignalBuilder::build() {
		size_t componentCount = _components.size();

        for (size_t i = 0; i < componentCount; ++i) {
            const Series &series = _components[i];
            if (series.empty() || series.startTime() != _startTime || series.endTime() != _endTime)
                throw std::runtime_error("Every series should have the same start and end time as the signal.");
        }

        return Signal(_startTime, _endTime, std::move(_components));
		// TODO: Maybe have a way to check if Builder is valid or not.
    }
}