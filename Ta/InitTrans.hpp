#pragma once

#include "CmdList.hpp"

namespace Ta {

    class State;

    class InitTrans {
    public:
        InitTrans(size_t id, State &dest, const CmdList &commands) : _id(id), _dest(dest), _commands(commands) {}

        size_t id() const {
            return _id;
        }

        const State &dest() const {
            return _dest;
        }

        const CmdList &commands() const {
            return _commands;
        }        

    private:
        size_t _id;
        State &_dest;
        CmdList _commands;

        friend class MutAutomaton;
    };
}