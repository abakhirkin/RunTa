#pragma once

#include "PropExpr.hpp"
#include "PropRef.hpp"

#include "Util.hpp"

#include <unordered_map>
#include <memory>

namespace Ta {    

    class PropSet : public U::CannotCopy {
    public:
        PropRef makeTrue();

        PropRef makeFalse();

        PropRef makeAtom(AtomRef atom);

        PropRef makeNot(PropRef arg);

        PropRef makeAnd(std::initializer_list<PropRef> args);

        PropRef makeOr(std::initializer_list<PropRef> args);

        const PropExpr &expr(PropRef ref) const;

    private:
        PropRef make(std::unique_ptr<PropExpr> expr);

        class PropBasePtrHash {
        public:
            size_t operator()(PropExpr *expr) const {
                return expr->hash();
            }
        };

        class PropBasePtrEqual {
        public:
            bool operator()(PropExpr *expr1, PropExpr *expr2) const {
                return expr1->equal(*expr2);
            }
        };

        std::unordered_map<PropExpr*, PropRef, PropBasePtrHash, PropBasePtrEqual> _exprToRef;
        std::vector<std::unique_ptr<PropExpr>> _idToExpr;
    };
}
