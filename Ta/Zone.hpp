#pragma once

#include "IfZone.hpp"

namespace Ta {

    typedef IfBound Bound;

    typedef IfZone Zone;

    typedef IfZonePrinter ZonePrinter;
}
