#pragma once

#include "Zone.hpp"
#include "CmdList.hpp"

#include "Util.hpp"

namespace Ta {

    class State;

    class Trans : public U::CannotCopy {
    public:
        Trans(size_t id, State &dest, const Zone &guard, const CmdList &commands)
            : _id(id), _dest(dest), _guard(guard), _commands(commands) {}

        size_t id() const {
            return _id;
        }

        const State &dest() const {
            return _dest;
        }

        const Zone &guard() const {
            return _guard;
        }

        const CmdList &commands() const {
            return _commands;
        }        

    private:
        size_t _id;
        State &_dest;
        Zone _guard;
        CmdList _commands;

        friend class MutAutomaton;
    };
}