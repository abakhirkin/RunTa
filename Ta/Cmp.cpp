#include "Cmp.hpp"

namespace Ta {

    std::ostream &operator<<(std::ostream &os, Cmp cmp) {
        switch (cmp) {
        case Cmp::LT:
            return os << "<";
        case Cmp::LEQ:
            return os << "<=";
        default:
            throw std::runtime_error("Invalid comparison type.");
        }
    }
}