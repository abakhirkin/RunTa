#pragma once

#include <ostream>
#include <stdexcept>

namespace Ta {
    /*! \brief Clock.

        Has an index of type size_t. A clock with index SIZE_MAX denotes constant 0 rather than an
        actual clock.
        
        Having size_t for index allows to transparently convert it to and from std::vector indices without
        type conversions (and thus thinking). We can have int or unsigned int indices if we want to. */
    class Clock {
    public:
        /*! \brief Creates a special clock that denotes constant 0.
        
            The index will be SIZE_MAX. */
        Clock() : _id(SIZE_MAX) {}

        /*! \brief Creates a clock with the given index.
        
            The index should not be SIZE_MAX. */
        explicit Clock(size_t id) : _id(id) {
            if (id == SIZE_MAX)
                throw std::out_of_range("Clock index out of range.");
        }

        /*! \brief Index of the clock.
        
            For constant 0, the index is SIZE_MAX. */
        size_t id() const {
            return _id;
        }

        /*! \brief Whenther the clock is constant 0. */
        bool is0() const {
            return _id == SIZE_MAX;
        }

        /*! \brief Equality. */
        bool operator==(Clock other) const {
            return _id == other._id;
        }

        /*! \brief Inequality. */
        bool operator!=(Clock other) const {
            return _id != other._id;
        }

    private:
        size_t _id;
    };

    /*! \breif Prints the clock to the stream.
    
        \relates Clock */
    std::ostream &operator<<(std::ostream &os, Clock c);
}
