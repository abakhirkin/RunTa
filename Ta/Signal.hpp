#pragma once

#include "Series.hpp"
#include "AtomRef.hpp"

#include "Util.hpp"

#include <vector>
#include <stdexcept>


namespace Ta {

    class Signal : public U::CannotCopy {
    // NOTE: deriving from CannotCopy prevents from accidentally copying signals, when moving is intended.
    // If we come to a need to copy signals, this can be removed.
    public:
        int startTime() const {
            return _startTime;
        }

        int endTime() const {
            return _endTime;
        }

        size_t size() const {
            return _components.size();
        }
        
        const Series &series(AtomRef atom) const {
            return _components[atom.id()];
        }

        const Series &operator[](AtomRef atom) const {
            return series(atom);
        }              
        
    private:
        int _startTime;
        int _endTime;        
        std::vector<Series> _components;

        template <typename... TArgs>
        Signal(int startTime, int endTime, TArgs&&... componentsArgs)
            : _startTime(startTime), _endTime(endTime), _components(std::forward<TArgs>(componentsArgs)...) {}

        friend class SignalBuilder;
    };
    
}
