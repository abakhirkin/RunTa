#include "Config.hpp"

namespace Ta {
    std::ostream &operator<<(std::ostream &os, const ConfigPrinter &printer) {
        const Config &config = printer.config();
        os << config.id() << " @ s" << config.state().id();
        os << printer.newline();         
        os << ZonePrinter(config.zone()).setNewline(printer.newline());
        return os;
    }

    std::ostream &operator<<(std::ostream &os, const Config &config) {
        return os << ConfigPrinter(config);
    }
}
