#include "Cmd.hpp"

#include <stdexcept>

namespace Ta {    

    std::ostream & operator<<(std::ostream & os, Cmd cmd) {
        switch (cmd.op()) {
        case Op::ASSIGN: {
            os << cmd.dest() << " := ";
            if (cmd.src().is0())
                os << cmd.value();
            else {
                os << cmd.src();
                if (cmd.value() != 0)
                    os << " + " << cmd.value();
            }
            return os;
        }

        case Op::FORGET: {
            return os << "forget " << cmd.dest();
        }

        default:
            throw std::runtime_error("Invalid command opcode.");
        }
    }
}
