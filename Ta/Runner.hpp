#pragma once

#include "Automaton.hpp"
#include "PropSignal.hpp"
#include "Config.hpp"
#include "ConfigSet.hpp"

#include <deque>
#include <ostream>

namespace Ta {

    class AcceptConfig : public U::CannotCopy {
    public:
        template<typename... TArgs>
        AcceptConfig(size_t id, const Config &parent, TArgs&&... zoneArgs)
            : _id(id), _parent(&parent), _zone(std::forward<TArgs>(zoneArgs)...) {}

        size_t id() const {
            return _id;
        }

        const Config &parent() const {
            return *_parent;
        }

        const Zone &zone() const {
            return _zone;
        }

    private:
        size_t _id;

        // Has to be a pointer, otherwise cannot assign (or move assign).
        const Config *_parent;
        
        Zone _zone;
    };

    class ContinueConfig {
    public:
        ContinueConfig(const Config &config, Config *parent) : _config(&config), _parent(parent) {}

        const Config &config() const {
            return *_config;
        }

        Config *parent() const {
            return _parent;
        }

    private:        
        const Config *_config;
        Config *_parent;
    };

    class Runner : public U::CannotCopy {
    public:
        Runner(const Automaton &ta, Clock timeClock);

        void run(PropSignal &signal);        

        size_t acceptConfigCount() const {
            return _acceptConfigs.size();
        }

        const AcceptConfig &acceptConfig(size_t i) const {
            return _acceptConfigs[i];
        }        

        size_t continueConfigCount() const {
            return _continueConfigs.size();
        }

        const Config &continueConfig(size_t i) const {
            return *_continueConfigs[i];
        }

        typedef std::list<Config>::const_iterator TConstConfigListIt;
        
        TConstConfigListIt configBegin() const {
            return _configs.begin();
        }

        TConstConfigListIt configEnd() const {
            return _configs.end();
        }

		size_t totalConfigCount() const {
			return _nextConfigId;
		}

    private:        
        void execInitTrans(PropSignal &signal, const InitTrans &trans);

        void execContinue(PropSignal &signal, Config &oldConfig);

        void addConfig(PropSignal &signal, Config &config, Config *parent);

        void execTrans(PropSignal &signal, Config &config, const Trans &trans);        

        size_t makeConfigId() {
            return _nextConfigId++;
        }        

        const Automaton &_ta;
        Clock _timeClock;      
        size_t _nextConfigId;
        std::deque<Config*> _worklist;
        ConfigSet _configSet;
        std::vector<AcceptConfig> _acceptConfigs;

        // Has to be a container where pointers are not invalidated when adding and removing
        // (for future, when we may start deleting useless configurations) elements.
        std::list<Config> _configs;        
        
        bool _continue;
        int _lastEndTime;
        std::vector<Config*> _continueConfigs;

        const size_t ACCEPT_CAPACITY = 16;
        const size_t CONTINUE_CAPACITY = 16;
    };

    class RunnerDotPrinter {
    public:
        RunnerDotPrinter(const Runner &runner) : _runner(runner) {}

        const Runner &runner() const {
            return _runner;
        }

    private:
        const Runner &_runner;
    };

    RunnerDotPrinter toDot(const Runner &runner);

    std::ostream &operator<<(std::ostream &os, RunnerDotPrinter &printer);    
}