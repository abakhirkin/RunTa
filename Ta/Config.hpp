#pragma once

#include "State.hpp"
#include "Zone.hpp"

#include "Util.hpp"

#include <vector>
#include <memory>
#include <list>

namespace Ta {

    class Config;

    /*! \brief Configuration of an automaton (State, Zone, Successors, etc). */
    class Config : public U::CannotCopy {
    public:
        template<typename... TArgs>
        Config(size_t id, const State &state, TArgs&&... zoneArgs)
            : _state(state), _zone(std::forward<TArgs>(zoneArgs)...), _covered(false), _coveredBy(nullptr), _expanded(false), parent(nullptr), _id(id) {
            _successors.reserve(SUCCESSORS_CAPACITY);
        }

        size_t id() const {
            return _id;
        }

        const State &state() const {
            return _state;
        }

        const Zone &zone() const {
            return _zone;
        }

        bool covered() const {
            return _covered;
        }

        Config *coveredBy() const {
            return _coveredBy;
        }

        void setCovered(Config *coveredBy) {
            _covered = true;
            _coveredBy = coveredBy;
        }

        bool expanded() const {
            return _expanded;
        }

        void setExpanded() {
            _expanded = true;
        }

        size_t successorCount() const {
            return _successors.size();
        }

        Config &successor(size_t i) const {
            return *_successors[i];
        }

        Config &successor(size_t i) {
            return *_successors[i];
        }


        void addSuccessor(Config &succ) {
            _successors.emplace_back(&succ);
        }

        bool initial() const {
            return !parent;
        }
        
    private:       
        const State &_state;
        Zone _zone;
        bool _covered;
        Config *_coveredBy;    
        bool _expanded;
    public:
        Config *parent;
    private:
        std::vector<Config*> _successors;
        size_t _id;

        const size_t SUCCESSORS_CAPACITY = 8;
    };

    class ConfigPrinter : public PrinterBase {
    public:
        ConfigPrinter(const Config &config) : _config(config) {}

        ConfigPrinter &setNewline(const char *newline) {
            PrinterBase::setNewline(newline);
            return *this;
        }

        const Config &config() const {
            return _config;
        }

    private:
        const Config &_config;
    };

    std::ostream &operator<<(std::ostream &os, const ConfigPrinter &printer);

    std::ostream &operator<<(std::ostream &os, const Config &config);
}
