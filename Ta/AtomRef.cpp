#include "AtomRef.hpp"

namespace Ta {

    std::ostream &operator<<(std::ostream & os, AtomRef atom) {
        return os << atom.id();
    }
}
