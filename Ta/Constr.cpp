#include "Constr.hpp"

namespace Ta {

    Constr::Constr(Clock pos, Clock neg, Cmp cmp, int value)
        : _pos(pos), _neg(neg), _cmp(cmp), _value(value) {
        // If the two clocks are the same: c - c <= b, setting the left side to 0.
        if (!_pos.is0() && !_neg.is0() && _pos == _neg) {
            _pos = Clock();
            _neg = Clock();
        }
        // Changing 0 < b to 0 <= 0 (canonical true) or 0 < 0 (canonical false).
        if (_pos.is0() && _neg.is0() && _value != 0) {
            if (cmpEval(cmp, 0, value)) {
                _cmp = Cmp::LEQ;
                _value = 0;
            }
            else {
                _cmp = Cmp::LT;
                _value = 0;
                return;
            }
        }
    }

    std::ostream &operator<<(std::ostream &os, Constr c) {
        if (c.isTrue())
            return os << "true";
        if (c.isFalse())
            return os << "false";

        if (!c.pos().is0())
            os << c.pos();
        if (!c.neg().is0())
            os << "-" << c.neg();
        if (c.pos().is0() && c.neg().is0())
            os << "0";
        return os << c.cmp() << c.value();
    }    
}
