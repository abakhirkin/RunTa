#pragma once

#include "CmdList.hpp"
#include "Zone.hpp"

namespace Ta {

    struct MutState;

    struct MutTrans {
        MutTrans(MutState &src, MutState &dest) : _src(src), _dest(dest), _guard(ZONE_TOP) {}       

        const MutState &dest() const {
            return _dest;
        }

        const Zone &guard() const {
            return _guard;
        }

        const CmdList &commands() const {
            return _commands;
        }

        MutState &_src;
        MutState &_dest;
        Zone _guard;
        CmdList _commands;
    };
}
