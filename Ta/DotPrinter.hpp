#pragma once

#include "MutAutomaton.hpp"

#include <ostream>
#include <unordered_map>

namespace Ta {

    /*! \brief Helper to print automata in dot format.
        
        An instance of DotPrinter stores a reference to an automaton.
        Printing a DotPrinter to a stream with operator<< prints the referred automaton.
        
        Can work with both Automaton and MutAutomaton (uses the accessor interface that they share). */
    template <typename TAutomaton>
    class DotPrinter {
    public:
        /*! \brief Creates a new helper instance that refers to the given automaton. */
        DotPrinter(const TAutomaton &ta) : _ta(ta) {}

        /*! \brief Associated automaton. */
        const TAutomaton &ta() const {
            return _ta;
        }

    private:
        const TAutomaton &_ta;
    };    

    /*! \brief Prints the automaton to the stream.

        \relates DotPrinter */
    template <typename TAutomaton>
    std::ostream &operator<<(std::ostream &os, DotPrinter<TAutomaton> printer) {
        os << "digraph {\nrankdir=LR;";

        const TAutomaton &ta = printer.ta();
        // NOTE: MutState-s do not have ids, so need a map for them.
        std::unordered_map<const TAutomaton::TState*, size_t> stateIds;
        for (size_t sI = 0, sCount = ta.stateCount(); sI < sCount; ++sI) {
            const TAutomaton::TState &state = ta.state(sI);            
            stateIds.emplace(&state, sI);
            os << "s" << sI << " [shape=\"box\", label=\"s" << sI << "\\n";
            if (!state.inputInv().isTrue())
                os << state.inputInv() << "\\n";
            if (!state.clockInv().isTop())
                os << ZonePrinter(state.clockInv()).setNewline("\\n") << "\\n";
            if (!state.allowTime())
                os << "No time\\n";
            if (!state.acceptCond().isEmpty())
                os << "Accept:\\n" << ZonePrinter(state.acceptCond()).setNewline("\\n");
            os << "\"];\n";
        }

        for (size_t sI = 0, sCount = ta.stateCount(); sI < sCount; ++sI) {
            const TAutomaton::TState &state = ta.state(sI);
            for (size_t tI = 0, tCount = state.outTransCount(); tI < tCount; ++tI) {
                const TAutomaton::TTrans &trans = state.outTrans(tI);
                size_t destId = stateIds.at(&trans.dest());
                os << "s" << sI << " -> " << "s" << destId << " [label=\"";
                if (!trans.guard().isTop())
                    os << ZonePrinter(trans.guard()).setNewline("\\n") << "\\n";
                os << CmdListPrinter(trans.commands()).setNewline("\\n") << "\"];\n";
            }
        }

        for (size_t tI = 0, tCount = ta.initTransCount(); tI < tCount; ++tI) {
            const TAutomaton::TInitTrans &trans = ta.initTrans(tI);
            size_t destId = stateIds.at(&trans.dest());
            os << "i" << tI << " [style=invisible];\n";
            os << "i" << tI << " -> " << "s" << destId << " [label=\"";
            os << CmdListPrinter(trans.commands()).setNewline("\\n") << "\"];\n";
        }

        os << "}";
        return os;
    }

    inline DotPrinter<Automaton> toDot(const Automaton &ta) {
        return DotPrinter<Automaton>(ta);
    }

    inline DotPrinter<MutAutomaton> toDot(const MutAutomaton &ta) {
        return DotPrinter<MutAutomaton>(ta);
    }
}
