#include "Series.hpp"

#include <stdexcept>

namespace Ta {   
        
    std::ostream & operator<<(std::ostream &os, const Series &series) {
        size_t size = series.size();
        
        if (size == 0)
            return os << "empty";

        if (size == 1) {
            Segment segment = series[0];
            return os << "[" << segment.endTime << "," << segment.endTime << "]" << " -> " << (int)segment.value;
        }

        int prevEndTime = series[0].endTime;
        for (size_t i = 1; i < size; ++i) {
            if (i > 1)
                os << "; ";
            Segment segment = series[i];            
            os << "[" << prevEndTime << "," << segment.endTime << "]" << " -> " << (int)segment.value;
            prevEndTime = segment.endTime;
        }
        return os;
    }
            
    std::pair<bool,size_t> Series::findTime(int time) const {
        size_t size = _segments.size();
        // Assuming there is at least 0th segment.        
        if (_segments[0].endTime > time)
            return std::make_pair(false, 0);
        size_t upper = size - 1;
        if (_segments[upper].endTime <= time)
            return std::make_pair(false, size);
        size_t lower = 1;

        while (true) {
            if (upper == lower)
                return std::make_pair(true, upper);
            size_t i = lower + (upper - lower) / 2;
            if (_segments[i].endTime > time)
                upper = i;
            else
                lower = i + 1;
        }
    }

    TrueIntervalsIt Series::findTrueIntervals(int startTime, int endTime) const {
        return TrueIntervalsIt(*this, startTime, endTime);
    }

    void Series::reserve(size_t capacity) {
        _segments.reserve(capacity);
    }

    void Series::add(int endTime, bool value) {
        size_t size = _segments.size();

        if (size == 0) {
            _segments.emplace_back(endTime, value);
            return;
        }

        Segment &last = _segments[size - 1];

        if (endTime < last.endTime)
            throw std::invalid_argument("New segment should end after the last one.");
        if (endTime == last.endTime && value != last.value)
            throw std::invalid_argument("If the new segment ends at the same time as the last one, it should have the same value.");

        if (endTime == last.endTime) {
        }
        else if (size > 1 && value == last.value) {
            last.endTime = endTime;
        }
        else {
            _segments.emplace_back(endTime, value);
        }                
    }
    
    TrueIntervalsIt::TrueIntervalsIt(const Series &series, int startTime, int endTime)
        : _series(series), _startTime(startTime), _endTime(endTime), _hasNext(true), _i(0) {
        if (startTime > endTime)
            throw std::invalid_argument("End time should be greater or equal to the start time.");

        if (_series.size() <= 1) {
            // If signal has duration 0.
            _hasNext = false;
            return;
        }

        // Here, series has at least 2 segments.
        std::pair<bool, size_t> startPos = _series.findTime(startTime);
        if (!startPos.first && startPos.second != 0) {
            // If start time points at or after the end of the signal.
            _hasNext = false;
            return;
        }

        if (startPos.first) {
            // If startTime points to a segment.
            _i = startPos.second - 1;
        }
        else /* if (startPos.second == 0) */ {
            // If start time points before the start of the signal.
            _i = 0;            
        }

        // At this point, _i points to the first segment that ends strictly after startTime.
        findNext();
    }
    
    // Moves _i to the next suitable segment.
    // Sets _hasNext to false, if there is no suitable segment.
    void TrueIntervalsIt::findNext() {
        if (!_hasNext)
            throw std::logic_error("Iterator does not have the next position.");
        // NOTE: We use strictly greater here.
        // When _endTime is a switching point, and the next segment has the value of 1,
        // we want to include [_endTime, _endTime] in the result.
        if (_series[_i].endTime > _endTime) {
            _hasNext = false;
            return;
        }
        ++_i;

        size_t size = _series.size();        
        while (true) {
            if (_i >= size) {
                _hasNext = false;
                return;
            }
            if (_series[_i].value)
                break;
            if (_series[_i].endTime > _endTime) {
                _hasNext = false;
                return;
            }
            ++_i;
        }
    }
    
    bool TrueIntervalsIt::hasNext() {
        return _hasNext;
    }
    
    SegmentInterval TrueIntervalsIt::moveNext() {
        SegmentInterval result(U::max(_series[_i - 1].endTime, _startTime), U::min(_series[_i].endTime, _endTime), _series[_i].endTime);
        findNext();
        return result;
    }  
    
}
