#pragma once

#include "PropRef.hpp"
#include "AtomRef.hpp"

#include <vector>
#include <ostream>
#include <algorithm>

namespace Ta {

    enum class PropOp {        
        NOT,
        AND,
        OR
    };

    class PropExpr {
    public:
        PropExpr(PropOp op) : _op(op) {}

        PropOp op() const {
            return _op;
        }

        virtual void print(std::ostream &os) const = 0;
        virtual bool equal(const PropExpr &other) const = 0;
        virtual size_t hash() const = 0;

    protected:
        PropOp _op;
    };

    std::ostream &operator<<(std::ostream &os, const PropExpr &prop);    

    class NotExpr : public PropExpr {
    public:
        NotExpr(PropRef arg) : PropExpr(PropOp::NOT), _arg(arg) {}

        PropRef arg() const {
            return _arg;
        }

        virtual void print(std::ostream &os) const;
        virtual bool equal(const PropExpr &other) const;
        virtual size_t hash() const;

    private:
        PropRef _arg;
    };

    class AndExpr : public PropExpr {
    public:
        AndExpr(std::initializer_list<PropRef> args);

        const std::vector<PropRef> &args() const {
            return _args;
        }

        virtual void print(std::ostream &os) const;
        virtual bool equal(const PropExpr &other) const;
        virtual size_t hash() const;

    private:
        std::vector<PropRef> _args;
    };

    class OrExpr : public PropExpr {
    public:
        OrExpr(std::initializer_list<PropRef> args);

        const std::vector<PropRef> &args() const {
            return _args;
        }

        virtual void print(std::ostream &os) const;
        virtual bool equal(const PropExpr &other) const;
        virtual size_t hash() const;

    private:
        std::vector<PropRef> _args;
    };
}

namespace std {

    template<>
    struct hash<Ta::PropExpr> {
        std::size_t operator()(const Ta::PropExpr &prop) const {
            return prop.hash();
        }
    };
}
