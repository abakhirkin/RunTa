#pragma once

#include "Util.hpp"

#include "MutState.hpp"
#include "MutTrans.hpp"
#include "MutInitTrans.hpp"

#include "Automaton.hpp"

#include <vector>
#include <memory>

namespace Ta {

    class MutAutomaton : public U::CannotCopy {
    public:
        typedef MutState TState;
        typedef MutTrans TTrans;
        typedef MutInitTrans TInitTrans;

        MutState &makeState() {
            std::unique_ptr<MutState> state = std::make_unique<MutState>();
            MutState &result = *state;
            _states.emplace_back(std::move(state));            
            return result;
        }

        MutTrans &makeTrans(MutState &src, MutState &dest) {
            std::unique_ptr<MutTrans> trans = std::make_unique<MutTrans>(src, dest);
            src.outgoing.push_back(trans.get());
            dest.incoming.push_back(trans.get());
            MutTrans &result = *trans;
            _transitions.emplace_back(std::move(trans));
            return result;
        }

        template<typename... TArgs>
        MutInitTrans &makeInit(MutState &state, TArgs&&... cmdListArs) {
            std::unique_ptr<MutInitTrans> init = std::make_unique<MutInitTrans>(state);
            init->_commands = CmdList(std::forward<TArgs>(cmdListArs)...);
            MutInitTrans &result = *init;
            _initTransitions.emplace_back(std::move(init));
            return result;
        }

        size_t stateCount() const {
            return _states.size();
        }

        MutState &state(size_t i) {
            return *_states[i];
        }

        const MutState &state(size_t i) const {
            return *_states[i];
        }

        size_t initTransCount() const {
            return _initTransitions.size();
        }

        MutInitTrans &initTrans(size_t i) {
            return *_initTransitions[i];
        }

        const MutInitTrans &initTrans(size_t i) const {
            return *_initTransitions[i];
        }

        Automaton build();

    private:
        std::vector<std::unique_ptr<MutState>> _states;
        std::vector<std::unique_ptr<MutTrans>> _transitions;
        std::vector<std::unique_ptr<MutInitTrans>> _initTransitions;
    };
}
