#pragma once

#include "If/ifzone.h"

namespace Ta {

	/*! \brief Helper for initializing IF zone module.

	The constructor initializes the IF zone module, and
	the destructor cleans it up. */
	class IfInit {
	public:
		IfInit() {
			if_init_zone_module();
			_topZone = if_universal_zone();
		}

		~IfInit() {
			if_exit_zone_module();
		}

		if_type_zone topZone() const {
			return _topZone;
		}

	private:
		if_type_zone _topZone;
	};
}
