#pragma once

namespace Ta {
    
    class PrinterBase {
    public:
        PrinterBase() : _newline("\n") {}

        PrinterBase setNewline(const char *newline) {
            _newline = newline;
            return *this;
        }

        const char *newline() const {
            return _newline.c_str();
        }

    protected:
        std::string _newline;
    };

}
