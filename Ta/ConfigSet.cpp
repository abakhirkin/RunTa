#include "ConfigSet.hpp"

namespace Ta {    
    
	
	void UnsortedStateConfigSet::insert(Config &newConfig) {
		for (size_t i = 0; i < _items.size(); ) {
			Config &configI = *_items[i];
			if (configI.covered()) {
				_items[i] = std::move(_items[_items.size() - 1]);
				_items.pop_back();
				continue;
			}
			if (newConfig.zone() <= configI.zone()) {
				newConfig.setCovered(&configI);
				return;
			}
			if (configI.zone() <= newConfig.zone()) {
				setCovered(configI, &newConfig);
				continue;
			}
			++i;
		}
		_items.push_back(&newConfig);
	}

	void UnsortedStateConfigSet::eraseCovered() {
		for (size_t i = 0; i < _items.size(); ) {
			if (_items[i]->covered()) {
				_items[i] = std::move(_items[_items.size() - 1]);
				_items.pop_back();
				continue;
			}
			++i;
		}
	}

	void UnsortedStateConfigSet::setCovered(Config &config, Config *by) {
		config.setCovered(by);
		for (size_t i = 0, count = config.successorCount(); i < count; ++i) {
			setCovered(config.successor(i), nullptr);
		}
	}
}
