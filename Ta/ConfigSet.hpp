#pragma once

#include "Config.hpp"
#include "Clock.hpp"

#include <vector>
#include <unordered_set>

namespace Ta {

	class UnsortedStateConfigSet {
	public:
		UnsortedStateConfigSet() {
			_items.reserve(RESERVE_ITEMS);
		}
		
		void insert(Config &config);

		typedef std::vector<Config*>::const_iterator TConstIt;

		TConstIt begin() const {
			return _items.begin();
		}

		TConstIt end() const {
			return _items.end();
		}

		void eraseCovered();

	private:
		std::vector<Config*> _items;
		void setCovered(Config &config, Config *by);

		const size_t RESERVE_ITEMS = 16;
	};

	struct StateConfigHash {
		size_t operator()(const Config *config) const {
			return config->zone().hash();
		}
	};

	struct StateConfigEqual {
		bool operator()(const Config *config1, const Config *config2) const {
			return config1->zone() == config2->zone();
		}
	};

	class StateConfigHashSet {
	public:
		StateConfigHashSet() {
			_set.rehash(RESERVE_BUCKETS);
		}

		void insert(Config &config) {
			auto result = _set.emplace(&config);
			if (!result.second) {
				// If the configuration already exists
				config.setCovered(*result.first);
			}
		}

		void clear() {
			_set.clear();
		}

		typedef std::unordered_set<Config*>::const_iterator TConstIt;

		TConstIt begin() const {
			return _set.begin();
		}

		TConstIt end() const {
			return _set.end();
		}

	private:
		std::unordered_set<Config*, StateConfigHash, StateConfigEqual> _set;

		// The exact number does not seem to matter, as the implementations will select it itself.
		// E.g., MSVC++ implementation doubles the current number of buckets until the number is reached.
		const size_t RESERVE_BUCKETS = 16;
	};

	typedef StateConfigHashSet StateConfigSet;

    /*! \brief Set of (pointers to) configurations that may belong to different states. */
    class ConfigSet {
    public:
        ConfigSet(size_t nStates, Clock sortClock) : _buckets(nStates, StateConfigSet()) {}

        void add(Config &config) {
            _buckets[config.state().id()].insert(config);
        }

        void clear() {
            _buckets = std::vector<StateConfigSet>(_buckets.size(), StateConfigSet());            
        }

    private:        
        std::vector<StateConfigSet> _buckets;
    };
}
