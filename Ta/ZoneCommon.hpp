#pragma once

namespace Ta {
    struct TopZoneTag {};
    struct EmptyZoneTag {};

    constexpr TopZoneTag ZONE_TOP;
    constexpr EmptyZoneTag ZONE_EMPTY;
}
