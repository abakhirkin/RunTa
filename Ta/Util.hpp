#pragma once

namespace U {
    template<typename T>
    T max(T v1, T v2) {
        return v1 > v2 ? v1 : v2;
    }

    template<typename T>
    T min(T v1, T v2) {
        return v1 < v2 ? v1 : v2;
    }

    bool and(bool b1, bool b2);

    bool or (bool b1, bool b2);

    class CannotCopy {
    public:
        constexpr CannotCopy() = default;
        ~CannotCopy() = default;

        CannotCopy(CannotCopy&&) = default;
        CannotCopy& operator=(CannotCopy&&) = default;

        CannotCopy(const CannotCopy&) = delete;
        CannotCopy& operator=(const CannotCopy&) = delete;
    };    

}
