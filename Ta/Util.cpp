#include "Util.hpp"

namespace U {
    bool and(bool b1, bool b2) {
        return b1 && b2;
    }

    bool or (bool b1, bool b2) {
        return b1 || b2;
    }
}

