#include "MutAutomaton.hpp"

#include <unordered_map>

namespace Ta {

    Automaton MutAutomaton::build() {
        std::unordered_map<MutState*, State*> stateMap;
        Automaton result;
        for (size_t i = 0, size = _states.size(); i < size; ++i) {
            MutState &state = *_states[i];
            result._states.emplace_back(i, state._inputInv, state._allowTime, state._acceptCond, state._clockInv);
        }
        for (size_t i = 0, size = _states.size(); i < size; ++i) {
            stateMap.emplace(_states[i].get(), &result._states[i]);
        }
        for (size_t i = 0, size = _transitions.size(); i < size; ++i) {
            MutTrans &trans = *_transitions[i];
            State &newSrc = *stateMap.at(&trans._src);
            State &newDest = *stateMap.at(&trans._dest);
            newSrc._outgoing.emplace_back(i, newDest, trans._guard, trans._commands);
        }
        for (size_t i = 0, size = _initTransitions.size(); i < size; ++i) {
            MutInitTrans &trans = *_initTransitions[i];
            State &newDest = *stateMap.at(&trans._dest);
            result._initTrans.emplace_back(i, newDest, trans._commands);
        }
        return result;
    }
}
