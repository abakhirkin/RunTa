#pragma once

#include "Zone.hpp"
#include "Trans.hpp"
#include "PropRef.hpp"

#include "Util.hpp"

#include <vector>

namespace Ta {

    class State : public U::CannotCopy {
    public:
        // Made so that it is easy to create dummy states for testing. Try not to use in actual code.
        explicit State(size_t id) : State(id, PROP_TRUE, true, Zone(ZONE_EMPTY), Zone(ZONE_TOP)) {}

        State(size_t id, PropRef inputInv, bool allowTime, const Zone &acceptCond, const Zone &clockInv)
            : _id(id), _inputInv(inputInv), _allowTime(allowTime), _acceptCond(acceptCond), _clockInv(clockInv) {}

        size_t id() const {
            return _id;
        }

        PropRef inputInv() const {
            return _inputInv;
        }        

        const Zone &clockInv() const {
            return _clockInv;
        }        

        const Zone &acceptCond() const {
            return _acceptCond;
        }

        bool allowTime() const {
            return _allowTime;
        }

        size_t outTransCount() const {
            return _outgoing.size();
        }

        Trans &outTrans(size_t i) {
            return _outgoing[i];
        }

        const Trans &outTrans(size_t i) const {
            return _outgoing[i];
        }          

    private:
        size_t _id;
        PropRef _inputInv;        
        bool _allowTime;
        Zone _acceptCond;
        std::vector<Trans> _outgoing;
        Zone _clockInv;

        friend class MutAutomaton;
    };
}
