#include "Clock.hpp"

namespace Ta {

    std::ostream &operator<<(std::ostream &os, Clock c) {
        if (c.is0())
            return os << "0";
        return os << "c" << c.id();
    }
}