#include "CmdList.hpp"

namespace Ta {
    std::ostream & Ta::operator<<(std::ostream &os, const CmdListPrinter &printer) {
        const CmdList &commands = printer.commands();
        for (size_t i = 0, size = commands.size(); i < size; ++i) {
            if (i > 0)
                os << printer.newline();
            os << commands[i];
        }
        return os;
    }

    std::ostream & Ta::operator<<(std::ostream & os, const CmdList &commands) {
        return os << CmdListPrinter(commands);
    }    

    bool CmdList::updatesClock(Clock c) const {
        for (size_t i = 0, size = _commands.size(); i < size; ++i) {
            if (_commands[i].dest() == c)
                return true;
        }
        return false;
    }
}
