#include "Runner.hpp"

#include "Util.hpp"

namespace Ta {

    Runner::Runner(const Automaton &ta, Clock timeClock)
        : _ta(ta), _timeClock(timeClock), _configSet(ta.stateCount(), timeClock), _continue(false), _lastEndTime(0) {
        if (timeClock.is0())
            throw std::invalid_argument("Time clock should not be constant 0.");

        for (size_t i = 0, count = _ta.initTransCount(); i < count; ++i) {
            const InitTrans &trans = _ta.initTrans(i);
            if (trans.commands().updatesClock(_timeClock))
                throw std::invalid_argument("Correct handling of transitions that modify time is not implemented.");
        }  

        for (size_t sI = 0, sCount = _ta.stateCount(); sI < sCount; ++sI) {
            const State &s = _ta.state(sI);
            for (size_t tI = 0, tCount = s.outTransCount(); tI < tCount; ++tI) {
                const Trans &trans = s.outTrans(tI);
                if (trans.commands().updatesClock(_timeClock))
                    throw std::invalid_argument("Correct handling of transitions that modify time is not implemented.");
            }
        }
        
        _acceptConfigs.reserve(ACCEPT_CAPACITY);
        _continueConfigs.reserve(CONTINUE_CAPACITY);
    }
    
    void Runner::run(PropSignal &signal) {
        if (signal.startTime() == signal.endTime())
            throw std::invalid_argument("Correct handling of 0-duration series is not implemented.");
        if (!_continue) {
            if (signal.startTime() != 0)
                throw std::invalid_argument("Signal should start at time 0.");
            for (size_t i = 0, count = _ta.initTransCount(); i < count; ++i) {
                execInitTrans(signal, _ta.initTrans(i));
            }
        }
        else {
            if (signal.startTime() != _lastEndTime)
                throw std::invalid_argument("Next signal should start where the previous signal ended.");
            // ASSUME: Worklist is empty here.
#ifdef _DEBUG
            if (!_worklist.empty())
                throw std::runtime_error("Assertion failure. Worklist should be empty when run() is called.");
#endif                  
            // NOTE: We need to clear _configs so that execContinue() starts to fill it again, but we want to keep
            // the elements valid until we finish with execContinue(), because they are referenced by the
			// elements of _continueConfigs.            
            std::list<Config> configs(std::move(_configs));
            _acceptConfigs.clear();
            _configSet.clear();
			// NOTE: execContinue() calls addConfig() which may add to _continueConfigs.
			// Thus, we move _continueConfigs before this loop.
            std::vector<Config*> continueConfigs(std::move(_continueConfigs));
            for (size_t i = 0, size = continueConfigs.size(); i < size; ++i) {
                execContinue(signal, *continueConfigs[i]);
            }            
        }

        // The index of the first AcceptConfig created during this call to run().
        size_t firstAcceptI = _acceptConfigs.size();
        while (!_worklist.empty()) {
            Config &config = *_worklist.front();
            _worklist.pop_front();
            if (config.covered())
                continue;
            // ASSUME: States on the worklist are not expanded.
#ifdef _DEBUG
            if (config.expanded())
                throw std::runtime_error("Assertion failure. A configuration on the worklist should not be expanded.");
#endif
            const State &state = config.state();
            for (size_t i = 0, count = state.outTransCount(); i < count; ++i) {                
                execTrans(signal, config, state.outTrans(i));
            }            
            config.setExpanded();
        }

        _lastEndTime = signal.endTime();
        _continue = true;

        for (size_t i = firstAcceptI; i < _acceptConfigs.size(); ) {
            if (_acceptConfigs[i].parent().covered()) {
                size_t size = _acceptConfigs.size();
                _acceptConfigs[i] = std::move(_acceptConfigs[size - 1]);
                _acceptConfigs.pop_back();
            }
            else
                ++i;            
        }
        for (size_t i = 0; i < _continueConfigs.size(); ) {            
            if (_continueConfigs[i]->covered()) {
                size_t size = _continueConfigs.size();
                _continueConfigs[i] = std::move(_continueConfigs[size - 1]);
                _continueConfigs.pop_back();
            }
            else
                ++i;
        }
    }
    
    void Runner::execInitTrans(PropSignal &signal, const InitTrans &trans) {        
        // Assume that commands do not change time clock.
        const State &initState = trans.dest();        
        const Series &series = signal[initState.inputInv()];
        std::pair<bool,size_t> seg0Id = series.findTime(0);
        if (!seg0Id.first) {
            throw std::invalid_argument("Correct handling of 0-duration series is not implemented.");
        }
        Segment seg0 = series[seg0Id.second];
        if (!seg0.value) {
            return;
        }
        // Here, the signal at time 0 satisfies the input invariant of the initial state.
        Zone initZone(ZONE_TOP);
        initZone.assign(_timeClock, 0);
        initZone.exec(trans.commands());
        initZone.intersect(initState.clockInv());
        if (initState.allowTime()) {
            initZone.timeElapse();
            initZone.intersect(Constr(_timeClock, Clock(), Cmp::LEQ, seg0.endTime));
            initZone.intersect(initState.clockInv());
        }        
        if (initZone.isEmpty()) {
            return;
        }
        Config &initConfig = _configs.emplace_back(makeConfigId(), initState, std::move(initZone));
        addConfig(signal, initConfig, nullptr);
    }

    void Runner::execContinue(PropSignal &signal, Config &oldConfig) {
        const State &state = oldConfig.state();
        const Series &series = signal[state.inputInv()];
        // NOTE: signal.startTime() is the same as the end time of the previous part.
        // This is checked before the call to execContinue().
		Zone newZone(oldConfig.zone());
		newZone.intersect(Constr(Clock(), _timeClock, Cmp::LEQ, -signal.startTime()));
        std::pair<bool, size_t> segId = series.findTime(signal.startTime());
        if (!segId.first)
            throw std::invalid_argument("Correct handling of 0-duration series is not implemented.");;
        Segment seg = series[segId.second];        
		if (seg.value && state.allowTime()) {
			newZone.timeElapse();
			newZone.intersect(Constr(_timeClock, Clock(), Cmp::LEQ, seg.endTime));
			newZone.intersect(state.clockInv());
		}
		// NOTE: Even if no time elapse can happen, we have to add the configuration to the worklist,
		// because there may be outgoing discrete transitions.
        Config &newConfig = _configs.emplace_back(makeConfigId(), state, std::move(newZone));        
        addConfig(signal, newConfig, nullptr);
    }

    void Runner::execTrans(PropSignal &signal, Config &config, const Trans &trans) {
        const State &destState = trans.dest();
        const Series &series = signal[destState.inputInv()];
        Zone destZone(config.zone());
        destZone.intersect(trans.guard());
        if (destZone.isEmpty())
            return;
		destZone.exec(trans.commands());
        // Assume that zones are closed when projected on time.
        // Assume that commands do not change time clock.
        int lower = -destZone.bound(Clock(), _timeClock).value();
        int upper = destZone.bound(_timeClock, Clock()).value();
        TrueIntervalsIt it = series.findTrueIntervals(lower, upper);
        while (it.hasNext()) {
            SegmentInterval interval = it.moveNext();
            Zone destZoneI(destZone);            
            destZoneI.intersect(Constr(Clock(), _timeClock, Cmp::LEQ, -interval.startTime));
            destZoneI.intersect(Constr(_timeClock, Clock(), Cmp::LEQ, interval.endTime));
            destZoneI.intersect(destState.clockInv());
            if (destState.allowTime()) {
                destZoneI.timeElapse();
                destZoneI.intersect(Constr(_timeClock, Clock(), Cmp::LEQ, interval.segmentEndTime));
                destZoneI.intersect(destState.clockInv());
            }
            if (destZoneI.isEmpty())
                continue;
            Config &newSucc = _configs.emplace_back(makeConfigId(), destState, std::move(destZoneI));            
            addConfig(signal, newSucc, &config);
        }
    }
    
    void Runner::addConfig(PropSignal &signal, Config &config, Config *parent) {        
        if (parent) {
            config.parent = parent;
            parent->addSuccessor(config);
        }        
        _configSet.add(config);
        if (config.covered())
            return;
        _worklist.push_back(&config);
        const Zone &acceptCond = config.state().acceptCond();
        if (!acceptCond.isEmpty()) {
            Zone acceptZone(config.zone());
            acceptZone.intersect(acceptCond);
            if (!acceptZone.isEmpty())
                _acceptConfigs.emplace_back(makeConfigId(), config, std::move(acceptZone));                
        }
        Bound timeBound = config.zone().bound(_timeClock, Clock());
        if (timeBound == Bound(Cmp::LEQ, signal.endTime()))
            _continueConfigs.emplace_back(&config);
    }

    void configToDotRec(std::ostream &os, const Config &config) {        
        os << "config" << config.id() << " [shape=\"box\", label=\"" << ConfigPrinter(config).setNewline("\\n");                
        if (config.covered()) {
            os << "\\nCovered";
        }
        if (!config.expanded()) {
            os << "\\nNot expanded";
        }
        os << "\"];\n";
        if (config.initial()) {
            os << "init" << config.id() << " [style=invisible];\n";
            os << "init" << config.id() << " -> " << "config" << config.id() << ";\n";
        }
        if (config.coveredBy()) {            
            os << "config" << config.id() << " -> " << "config" << config.coveredBy()->id() << "[style=\"dotted\"];\n";
        }        
        for (size_t i = 0, count = config.successorCount(); i < count; ++i) {
            const Config &succ = config.successor(i);
            os << "config" << config.id() << " -> " << "config" << succ.id() << ";\n";
        }
    }
    
    RunnerDotPrinter toDot(const Runner &runner) {        
        return RunnerDotPrinter(runner);
    }

    std::ostream &operator<<(std::ostream &os, RunnerDotPrinter &printer) {
        const Runner &runner = printer.runner();

        os << "digraph {\n";

        for (auto it = runner.configBegin(), end = runner.configEnd(); it != end; ++it) {                       
            configToDotRec(os, *it);
        }

        for (size_t i = 0, count = runner.acceptConfigCount(); i < count; ++i) {
            const AcceptConfig &accept = runner.acceptConfig(i);
            os << "accept" << accept.id() << " [shape=\"box\", label=\"Accept " << accept.id() << " @ s" << accept.parent().state().id() << "\\n"
               << ZonePrinter(accept.zone()).setNewline("\\n") << "\"];\n";
            // Direction of the edge w.r.t. "->" influences the layout, and
            // the result looks cleaner when "->" goes from a Config to an AcceptConfig
            // But since this edge is similar to a cover edge, we use dir="back"
            // to make the drawn arrow point towards Config.
            os << "config" << accept.parent().id() << " -> " << "accept" << accept.id()  << " [style=\"dotted\", dir=\"back\"];\n";
        }

        for (size_t i = 0, count = runner.continueConfigCount(); i < count; ++i) {
            const Config &config = runner.continueConfig(i);
            os << "cont" << config.id() << "[shape=plaintext, label=\"Continue\"];\n";
            os << "config" << config.id() << " -> " << "cont" << config.id() << ";\n";
        }

        os << "}";
        return os;
    }
}
