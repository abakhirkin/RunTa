#include "Util.hpp"

#include "PropSignal.hpp"

namespace Ta {

    PropSignal::PropSignal(const Signal &signal, const PropSet &propSet)
        : _startTime(signal.startTime()), _endTime(signal.endTime()),
          _signal(signal), _propSet(propSet) {

        _trueSeries = makeTrueSeries();
        _falseSeries = makeFalseSeries();
    }
    
    const Series &PropSignal::series(PropRef ref) {        
        switch (ref.type()) {
        case PropRefType::FALSE:
            return *_falseSeries.get();
        case PropRefType::TRUE:
            return *_trueSeries.get();
        case PropRefType::ATOM:
            return _signal.series(AtomRef(ref.id()));
        default:
            break;
        }
        // At this point, PropRefType is EXPR.
		if (ref.id() < _propSeries.size()) {
			Series* series = _propSeries[ref.id()].get();
			if (series) {
				return *series;
			}
		}		
		// At this point, requested id is not in _propSeries.		       

        const PropExpr &prop = _propSet.expr(ref);

        std::unique_ptr<Series> series;
        switch (prop.op())
        {
        case PropOp::NOT:
            series = makeNotSeries(static_cast<const NotExpr&>(prop));
            break;

        case PropOp::AND:
            series = makeAndSeries(static_cast<const AndExpr&>(prop));
            break;

        case PropOp::OR:
            series = makeOrSeries(static_cast<const OrExpr&>(prop));
            break;

        default:
            throw std::invalid_argument("Unsupported proposition type.");
        }

        ensureSize(ref);
        _propSeries[ref.id()] = std::move(series);
        return *_propSeries[ref.id()];
    }

    void PropSignal::ensureSize(PropRef prop) {
        while (_propSeries.size() < prop.id() + 1)
            _propSeries.emplace_back();
    }
    
    std::unique_ptr<Series> PropSignal::makeConstSeries(bool value) {
        std::unique_ptr<Series> result = std::make_unique<Series>(SERIES_EMPTY);
        result->reserve(2);
        result->add(_startTime, value);
        if (_endTime > _startTime)
            result->add(_endTime, value);
        return result;
    }

    std::unique_ptr<Series> PropSignal::makeTrueSeries() {
        return makeConstSeries(true);
    }
    
    std::unique_ptr<Series> PropSignal::makeFalseSeries() {
        return makeConstSeries(false);
    }
    
    std::unique_ptr<Series> PropSignal::makeNotSeries(const NotExpr &prop) {
        const Series &arg = series(prop.arg());
        std::unique_ptr<Series> result = std::make_unique<Series>(SERIES_EMPTY);
        result->reserve(arg.size());
        for (size_t i = 0, size = arg.size(); i < size; ++i) {
            Segment segment = arg[i];
            result->add(segment.endTime, !segment.value);
        }
        return result;
    }       

    std::unique_ptr<Series> PropSignal::makeAndSeries(const AndExpr &prop) {
        return makeBinopSeries(prop.args(), U::and, true);
    }

    std::unique_ptr<Series> PropSignal::makeOrSeries(const OrExpr &prop) {
        return makeBinopSeries(prop.args(), U::or, false);
    }

    struct SeriesIndex {
        const Series *series;
        size_t index;
    };
    
    std::unique_ptr<Series> PropSignal::makeBinopSeries
        (const std::vector<PropRef>& argRefs, bool(*binop)(bool, bool), bool accInit) {
        
        size_t argsSize = argRefs.size();
        if (argsSize == 0)
            return makeConstSeries(accInit);

        // TODO: Perhaps make this static or a class field.
        std::vector<SeriesIndex> args;
        args.reserve(argsSize);
        size_t maxSize = 0;
        for (size_t i = 0; i < argsSize; ++i) {
            const Series &arg = series(argRefs[i]);
            maxSize = U::max(maxSize, arg.size());
            args.push_back({ &arg, 0 });
        }

        std::unique_ptr<Series> result = std::make_unique<Series>(SERIES_EMPTY);
        result->reserve(maxSize);
        bool end = false;
        while (!end) {
            int minEndTime = INT_MAX;
            // NOTE: We assume that each argument series has at least one segment.
            for (size_t i = 0; i < argsSize; ++i) {
                SeriesIndex arg = args[i];
                minEndTime = U::min(minEndTime, arg.series->segment(arg.index).endTime);
            }
            bool acc = accInit;
            for (size_t i = 0; i < argsSize; ++i) {
                SeriesIndex &arg = args[i];
                Segment segment = arg.series->segment(arg.index);
                acc = binop(acc, segment.value);
                if (segment.endTime == minEndTime) {                    
                    ++arg.index;
                    if (arg.index >= arg.series->size())
                        end = true;
                }
            }
            result->add(minEndTime, acc);
        }

        return result;
    }
}
