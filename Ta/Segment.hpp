#pragma once

namespace Ta {

    /*! \brief Segment of a Boolean series.
        
        Stores an integer end time and a Boolean value.
        
        Later we can make it into a template, with time type and value type being the parameters. */
    struct Segment {
        /*! \brief Creates a new segment with the given end time and value. */
        Segment(int endTime, bool value) : endTime(endTime), value(value) {}

        /*! \brief End time. */
        int endTime;

        /*! \brief Value. */
        bool value;
    };
}
