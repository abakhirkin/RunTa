#pragma once

#include <ostream>

namespace Ta {
    /*! \brief Bounded time interval.
        
        We do not specify whether the bounds are open or closed, but printing code uses closed notation. */
    struct Interval {
        /*! \brief Creates a new time interval with the given start and end time. */
        Interval(int startTime, int endTime) : startTime(startTime), endTime(endTime) {}
        
        /*! \brief Start time. */
        int startTime;
        
        /*! \brief End time. */
        int endTime;

        /*! \brief Equality. */
        bool operator==(Interval other) const {
            return
                startTime == other.startTime &&
                endTime == other.endTime;
        }

        /*! \brief Inequality. */
        bool operator!=(Interval other) const {
            return
                startTime != other.startTime ||
                endTime != other.endTime;
        }
    };

    /*! \brief Prints the interval to the stream.
    
        \relates Interval */
    std::ostream &operator<<(std::ostream &os, Interval interval);
}
