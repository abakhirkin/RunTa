#include "PropRef.hpp"

namespace Ta {

    std::ostream &operator<<(std::ostream &os, PropRef prop) {
        switch (prop.type()) {
        case PropRefType::TRUE:
                return os << "true";
        case PropRefType::FALSE:
            return os << "false";
        case PropRefType::ATOM:
            return os << "b" << prop.id();
        case PropRefType::EXPR:
            return os << "p" << prop.id();
        default:
            throw std::invalid_argument("Unsupported proposition type.");
        }
    }
}