#pragma once

#include "Cmp.hpp"

#include "If/ifzone.h"

namespace Ta {
    class IfBound {
    public:
        explicit IfBound(int bound) : _bound(bound) {}

        IfBound(Cmp cmp, int value) : _bound(if_make_bound(value, (int)cmp)) {}

        int value() const {
            return if_value_bound(_bound);
        }

        int isLtInf() const {
            return _bound == IF_BOUND_INFTY;
        }

        bool operator==(IfBound other) const {
            return _bound == other._bound;
        }

        bool operator!=(IfBound other) const {
            return _bound != other._bound;
        }

        bool operator<=(IfBound other) const {
            return _bound <= other._bound;
        }

        bool operator>=(IfBound other) const {
            return _bound >= other._bound;
        }

        bool operator<(IfBound other) const {
            return _bound < other._bound;
        }

        bool operator>(IfBound other) const {
            return _bound > other._bound;
        }

        bool isNegDisjoint(IfBound negBound) {
            if (_bound == IF_BOUND_INFTY || negBound._bound == IF_BOUND_INFTY)
                return false;
            int upper = if_value_bound(_bound);
            int otherLower = -if_value_bound(negBound._bound);
            return
                (upper < otherLower) ||
                (upper == otherLower && (if_sign_bound(_bound) == 0 || if_sign_bound(negBound._bound) == 0));
        }

    private:
        int _bound;
    };
}
