#pragma once

#include "PropSet.hpp"
#include "Signal.hpp"

#include <vector>
#include <memory>

namespace Ta {

    class PropSignal : public U::CannotCopy {
    public:
        PropSignal(const Signal &signal, const PropSet &propSet);

        int startTime() const {
            return _startTime;
        }

        int endTime() const {
            return _endTime;
        }

        const Series &operator[](PropRef prop) {
            return series(prop);
        }

        const Series &series(PropRef prop);

    private:
        void ensureSize(PropRef prop);

        std::unique_ptr<Series> makeConstSeries(bool value);

        std::unique_ptr<Series> makeTrueSeries();

        std::unique_ptr<Series> makeFalseSeries();

        std::unique_ptr<Series> makeNotSeries(const NotExpr &prop);        

        std::unique_ptr<Series> makeBinopSeries(const std::vector<PropRef> &argRefs, bool(*binop)(bool, bool), bool accInit);

        std::unique_ptr<Series> makeAndSeries(const AndExpr &prop);

        std::unique_ptr<Series> makeOrSeries(const OrExpr &prop);        

        int _startTime;
        int _endTime;
        const Signal &_signal;

        const PropSet &_propSet;
        std::vector<std::unique_ptr<Series>> _propSeries;
        std::unique_ptr<Series> _trueSeries;
        std::unique_ptr<Series> _falseSeries;
    };
}
