#pragma once

#include "Clock.hpp"

#include <ostream>

namespace Ta {
        
    /*! \brief Type of the tag to create clock reset command. */
    struct ResetCmdTag {};
    /*! \brief Type of the tag to create clock assign command. */
    struct AssignCmdTag {};
    /*! \brief Type of the tag to create clock forget command. */
    struct ForgetCmdTag {};

    /*! \brief Tag to create clock reset command. */
    constexpr ResetCmdTag CMD_RESET;
    /*! \brief Tag to create clock assign command. */
    constexpr AssignCmdTag CMD_ASSIGN;
    /*! \brief Tag to create clock forget command. */
    constexpr ForgetCmdTag CMD_FORGET;

    /*! \brief Clock update opcode. */
    enum class Op {
        /*! \brief Assign command. */
        ASSIGN = 0,
        /*! \brief Forget command. */
        FORGET = 1
    };

    /*! \brief Clock update command. */
    class Cmd {
    public:
        
        /*! \brief Creates a clock reset command.
        
            Creates the clock assign command \a dest = 0.
            Sets the opcode to Op::ASSIGN.
            Sets the destination clock to \a dest.
            Sets the source clock to constant-0-clock.
            Sets the source value to 0. */
        Cmd(ResetCmdTag, Clock dest) : Cmd(Op::ASSIGN, dest, Clock(), 0) {}        

        /*! Creates a clock assign command.
            
            Creates the clock assign command \a dest = \a src + \a value.
            Sets the opcode to Op::ASSIGN.
            Sets the destination clock to \a dest.
            Sets the source clock to \a src.
            Sets the source value to \a value. */
        Cmd(AssignCmdTag, Clock dest, Clock src, int value = 0) : Cmd(Op::ASSIGN, dest, src, value) {}        

        /*! \brief Creates a clock assign command.

            Creates the clock assign command \a dest = \a value.
            Sets the opcode to Op::ASSIGN.
            Sets the destination clock to \a dest.
            Sets the source clock to constant-0-clock.
            Sets the source value to \a value. */
        Cmd(AssignCmdTag, Clock dest, int value) : Cmd(Op::ASSIGN, dest, Clock(), value) {}

        /*! \brief Creates a clock forget command.
            
            Forgets (existentially quantifies) \a dest.
            Sets the opcode to Op::FORGET.
            Sets the destination clock to \a dest. */
        Cmd(ForgetCmdTag, Clock dest) : Cmd(Op::FORGET, dest, Clock(), 0) {}

        /*! \brief Command opcode. */
        Op op() const {
            return _op;
        }

        /*! \brief Destination clock of the command (clock to be modified). */
        Clock dest() const {
            return _dest;
        }

        /*! \brief Source clock of the command.
            
            For the assign command, may be the constant-0-clock.
            For the forget command, unspecified. */
        Clock src() const {
            return _src;
        }

        /*! \brief Source constant value of the command.
        
            For the forget command, unspecified.*/
        int value() const {
            return _value;
        }

    private:        
        Cmd(Op op, Clock dest, Clock src, int value) : _op(op), _src(src), _dest(dest), _value(value) {}

        Op _op;
        Clock _dest;
        Clock _src;
        int _value;
    };

    /*! \brief Prints the command to the stream.
        
        \relates Cmd */
    std::ostream &operator<<(std::ostream &os, Cmd cmd);
}
