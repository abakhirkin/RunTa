#include "PropExpr.hpp"

namespace Ta {

    std::ostream &operator<<(std::ostream & os, const PropExpr & prop) {
        prop.print(os);
        return os;
    }   
   
    void NotExpr::print(std::ostream &os) const {
        os << "(not " << _arg << ")";
    }
    
    bool NotExpr::equal(const PropExpr &other) const {
        if (other.op() != PropOp::NOT)
            return false;
        return _arg == static_cast<const NotExpr&>(other)._arg;
    }

    size_t NotExpr::hash() const {
        size_t result = 17;
        result = result * 31 + (size_t)_op;        
        result = result * 31 + _arg.id();
        return result;
    }
    
    AndExpr::AndExpr(std::initializer_list<PropRef> args) : PropExpr(PropOp::AND), _args(args) {
        std::sort(_args.begin(), _args.end());
        _args.erase(std::unique(_args.begin(), _args.end()), _args.end());
    }

    void AndExpr::print(std::ostream &os) const {
        os << "(and ";        
        for (size_t i = 0, size = _args.size(); i < size; ++i) {
            if (i != 0)
                os << " ";
            os << _args[i];
        }
        os << ")";
    }
    
    bool AndExpr::equal(const PropExpr &other) const {
        if (other.op() != PropOp::AND)
            return false;
        return _args == static_cast<const AndExpr&>(other)._args;
    }

    size_t AndExpr::hash() const {
        size_t result = 17;
        result = result * 31 + (size_t)_op;
        for (size_t i = 0, size = _args.size(); i < size; ++i) {
            result = result * 31 + _args[i].id();
        }
        return result;
    }

    OrExpr::OrExpr(std::initializer_list<PropRef> args) : PropExpr(PropOp::OR), _args(args) {
        std::sort(_args.begin(), _args.end());
        _args.erase(std::unique(_args.begin(), _args.end()), _args.end());
    }

    void OrExpr::print(std::ostream &os) const {
        os << "(or ";
        for (size_t i = 0, size = _args.size(); i < size; ++i) {
            if (i != 0)
                os << " ";
            os << _args[i];
        }
        os << ")";
    }

    bool OrExpr::equal(const PropExpr &other) const {
        if (other.op() != PropOp::OR)
            return false;
        return _args == static_cast<const OrExpr&>(other)._args;
    }
    
    size_t OrExpr::hash() const {
        size_t result = 17;
        result = result * 31 + (size_t)_op;
        for (size_t i = 0, size = _args.size(); i < size; ++i) {
            result = result * 31 + _args[i].id();
        }
        return result;
    }
}
