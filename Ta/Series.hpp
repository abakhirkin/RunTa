#pragma once

#include "Util.hpp"
#include "Segment.hpp"
#include "Interval.hpp"

#include <vector>
#include <stdexcept>
#include <ostream>

namespace Ta {   

    struct SegmentInterval {
        int startTime;
        int endTime;
        int segmentEndTime;

        SegmentInterval(int startTime, int endTime, int segmentEndTime)
            : startTime(startTime), endTime(endTime), segmentEndTime(segmentEndTime) {}

        bool operator==(SegmentInterval other) const {
            return startTime == other.startTime && endTime == other.endTime && segmentEndTime == other.segmentEndTime;
        }

        bool operator!=(SegmentInterval other) const {
            return startTime != other.startTime || endTime != other.endTime || segmentEndTime != other.segmentEndTime;
        }
    };

    class Series;

    class TrueIntervalsIt {
    public:
        TrueIntervalsIt(const Series &series, int startTime, int endTime);

        bool hasNext();

        SegmentInterval moveNext();

    private:
        void findNext();

        const Series &_series;
        int _startTime;
        int _endTime;        
        bool _hasNext;
        size_t _i;        
    };
    
    struct EmptySeriesTag {};
    constexpr EmptySeriesTag SERIES_EMPTY;


    /*! \brief Boolean signal.
    
        Logically, a series is a sequence of adjacent intervals of non-zero duration, each mapped to a
        Boolean value.
        
        Series is implemented as an std::vector of Segment. I-th segment gives the end time and value of the
        (i-1)-th logical interval, and (i-1)-th Segment gives its start time.
        The start time of the signal is given by 0-th segment.
        Series itself does not specify the semantics of the switching points (what is the value at the
        swithcing points and whether it is defined at all).

        For example, the signal [0, 1] -> 1; [1, 2] -> 0 (a signal that has value 1 between time 0 and 1,
        and value 0 between time 1 and 2) is represented as a vector of Segment (0, ?), (1, 1), (2, 0).
        The value stored in the 0-th segment does not matter in this case.
        
        In a valid series, 0-th segment should always be present. Algorithms that read Series assume this.
        Passing SERIES_EMPTY to the constructor creates an invalid empty series (with no segments).
        Methods that construct Series (e.g., add segments to it) expect that 0-th segment may not be present.
        
        Deriving from CannotCopy prevents from accidentally copying series, when moving is intended.
        If we come to a need to copy series, this can be removed.
        
        Later we can make it into a template, with time type and value type being the parameters. */
    class Series : public U::CannotCopy{    
    public:
        /*! \brief Creates an invalid empty series (with no segments). */
        Series(EmptySeriesTag) {}

        /*! \brief Creates a new series with the given startTime and of duration 0.
        
            Optional argument \value allows to set the value field of the 0-th segment. */
        Series(int startTime, bool value = 0) {
            add(startTime, value);
        }

        /*! \brief Creates a new series starting at time 0 and of duration 0. */
        Series() : Series(0) {}        

        /*! \brief Start time of the series. */
        int startTime() const {            
            return _segments[0].endTime;
        }

        /*! \brief End time of the series. */
        int endTime() const {            
            return _segments[_segments.size() - 1].endTime;
        }

        /*! \brief Number of segments in the series.
            
            Including the 0-th one. Thus, an invalid empty series has size 0.
            A valid series of duration 0 has one segment.
            A series of nonzero duration has at lest two segments. */
        size_t size() const {
            return _segments.size();
        }

        /*! \brief I-th segment of the series.
            
            0-th segment stores the start time. */
        Segment segment(size_t i) const {
            return _segments[i];
        }

        /*! Same as Series::segment(). */
        Segment operator[](size_t i) const {
            return segment(i);
        }

        // TODO: Series::findTime() and Series::findTrueIntervals may need to be moved elsewhere,
        // since they assume specific semantics of the switching points.
        /*! \brief Finds the index of the first segment that ends strictly after the given time. */
        std::pair<bool,size_t> findTime(int time) const;

        /*! \brief Finds intervals between two given time points where the signal has value 1. */
        TrueIntervalsIt findTrueIntervals(int startTime, int endTime) const;

        /*! \brief Sets the capacity of the inner vector that stores Segment instances.
        
            Correctly works with invalid empty series. */
        void reserve(size_t capacity);

        /*! \brief Adds a new segment to the series.

            The new segment should end strictly after the last existing one, in which case it is added;
            or have the same end time and value as the existing last one in which case the series is unchanged.
            Otherwise the method throws an exception.
        
            Correctly works with invalid empty series.*/
        void add(int endTime, bool value);

        bool empty() const {
            return _segments.empty();
        }

    private:
        std::vector<Segment> _segments;
    };

    /*! \brief Prints the series to the stream.
        
        \relates Series */
    std::ostream &operator<<(std::ostream &os, const Series &series);
}
