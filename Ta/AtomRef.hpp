#pragma once

#include <ostream>

namespace Ta {

    /*! \brief Reference to a signal component.

        Stores an index of a signal component of the type size_t.
        
        Having size_t for index allows to transparently convert it to and from std::vector indices without
        type conversions (and thus thinking). We can have int out unsigned int indices if we want to. */
    class AtomRef {
    public:       
        /*! \brief Creates a new reference to the component with the given index. */
        explicit AtomRef(size_t id) : _id(id) {}

        /*! \brief Index of the component. */
        size_t id() const {
            return _id;
        }

        /*! \brief Equality. */
        bool operator==(AtomRef other) const {
            return _id == other._id;
        }

        /*! \brief Inequality. */
        bool operator!=(AtomRef other) const {
            return _id != other._id;
        }

    private:
        size_t _id;
    };

    /*! \brief Prints the component index to the stream.
    
        \relates AtomRef */
    std::ostream &operator<<(std::ostream &os, AtomRef atom);
}