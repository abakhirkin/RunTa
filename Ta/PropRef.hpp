#pragma once

#include "AtomRef.hpp"

#include <stdexcept>
#include <ostream>

namespace Ta {

    struct TruePropTag {};
    struct FalsePropTag {};

	struct ExprPropTag {};

	constexpr ExprPropTag PROP_EXPR{};

    enum class PropRefType {
        FALSE,
        TRUE,
        ATOM,
        EXPR
    };

    class PropRef {
    public:
        explicit constexpr PropRef(TruePropTag) : _type(PropRefType::TRUE), _id(0) {}

        explicit constexpr PropRef(FalsePropTag) : _type(PropRefType::FALSE), _id(0) {}

        explicit PropRef(AtomRef atom) : _type(PropRefType::ATOM), _id(atom.id()) {}

        PropRef(ExprPropTag, size_t id) : _type(PropRefType::EXPR), _id(id) {}

        bool isTrue() const {
            return _type == PropRefType::TRUE;
        }

        bool isFalse() const {
            return _type == PropRefType::FALSE;
        }

        bool isAtom() const {
            return _type == PropRefType::ATOM;
        }

        bool isExpr() const {
            return _type == PropRefType::EXPR;
        }

        PropRefType type() const {
            return _type;
        }

        size_t id() const {
            return _id;
        }

        /*! \brief Equality. */
        bool operator==(PropRef other) const {
            return _type == other._type && _id == other._id;
        }

        /*! \brief Inequality. */
        bool operator!=(PropRef other) const {
            return  _type == other._type || _id != other._id;
        }

        /*! \brief Comparison. */
        bool operator<(PropRef other) const {
            if (_type != other._type)
                return _type < other._type;
            return _id < other._id;
        }

    private:
        PropRefType _type;
        size_t _id;
    };

    constexpr PropRef PROP_TRUE { TruePropTag() };

    constexpr PropRef PROP_FALSE { FalsePropTag() };

    std::ostream &operator<<(std::ostream &os, PropRef prop);
}
