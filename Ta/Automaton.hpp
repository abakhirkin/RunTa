#pragma once

#include "Util.hpp"
#include "State.hpp"
#include "InitTrans.hpp"

#include <vector>

namespace Ta {

    /*! \brief Immutable automaton.
    
        Immutable automaton to be used in simulation.

        To create an instance of Automaton, first construct a MutAutomaton and then call MutAutomaton::build().

        Automaton and MutAutomaton have compatible accessor interface.
        
        Deriving from CannotCopy prevents from accidentally copying automata, when moving is intended.
        If we come to a need to copy automata, this can be removed. */
    class Automaton : public U::CannotCopy {    
    public:
        /*! \brief Type of the state of the automaton. */
        typedef State TState;

        /*! \brief Type of the transition of the automaton. */
        typedef Trans TTrans;

        /*! \brief Type of the initial transition of the automaton. */
        typedef InitTrans TInitTrans;

        /*! \brief Creates an empty automaton.*/
        Automaton() {}

        /*! \brief Number of states.
            
            Part of the accessor interface that is shared with MutAutomaton. */
        size_t stateCount() const {
            return _states.size();
        }

        /*! \brief I-th state.
        
            Part of the accessor interface that is shared with MutAutomaton.*/
        State &state(size_t i) {
            return _states[i];
        }

        /*! \brief I-th state.
            
            Part of the accessor interface that is shared with MutAutomaton. */
        const State &state(size_t i) const {
            return _states[i];
        }

        /*! \brief Number of initial transitions.
        
            Part of the accessor interface that is shared with MutAutomaton.*/
        size_t initTransCount() const {
            return _initTrans.size();
        }

        /*! I-th initial transition.
        
            Part of the accessor interface that is shared with MutAutomaton. */
        InitTrans &initTrans(size_t i) {
            return _initTrans[i];
        }

        /*! I-th initial transition.
        
            Part of the accessor interface that is shared with MutAutomaton. */
        const InitTrans &initTrans(size_t i) const {
            return _initTrans[i];
        }
        
    private:
        std::vector<State> _states;
        std::vector<InitTrans> _initTrans;

        friend class MutAutomaton;
    };
}