#pragma once

#include "Signal.hpp"

namespace Ta {

    class SignalBuilder : public U::CannotCopy {
    public:
        SignalBuilder(int startTime, int endTime, size_t componentCount);

        Series &series(AtomRef atom) {
            return _components[atom.id()];
        }

        Series &operator[](AtomRef atom) {
            return series(atom);
        }

        Signal build();

    private:		
        int _startTime;
        int _endTime;
        std::vector<Series> _components;
    };
}
