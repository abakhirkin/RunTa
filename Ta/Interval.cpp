#include "Interval.hpp"

namespace Ta {

    std::ostream &operator<<(std::ostream & os, Interval interval) {
        return os << "[" << interval.startTime << ", " << interval.endTime << "]";
    }
}
