#pragma once

#include "CmdList.hpp"

namespace Ta {

    struct MutState;

    struct MutInitTrans {
        MutInitTrans(MutState &dest) : _dest(dest) {}

        const MutState &dest() const {
            return _dest;
        }

        const CmdList &commands() const {
            return _commands;
        }

        MutState &_dest;
        CmdList _commands;
    };
}
