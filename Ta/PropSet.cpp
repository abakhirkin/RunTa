#include "PropSet.hpp"

namespace Ta {
    
    PropRef PropSet::makeTrue() {
        return PROP_TRUE;
    }
    
    PropRef PropSet::makeFalse() {
        return PROP_FALSE;
    }

    PropRef PropSet::makeAtom(AtomRef atom) {
        return PropRef(atom);
    }

    PropRef PropSet::makeNot(PropRef arg) {
        std::unique_ptr<PropExpr> prop = std::make_unique<NotExpr>(arg);
        return make(std::move(prop));
    }

    PropRef PropSet::makeAnd(std::initializer_list<PropRef> args) {
        // TODO: Perhaps, do at least some canonicalization here.
        // AndProp and OrProp constructors do some, but not much.
        std::unique_ptr<PropExpr> prop = std::make_unique<AndExpr>(args);
        return make(std::move(prop));
    }

    PropRef PropSet::makeOr(std::initializer_list<PropRef> args) {
        std::unique_ptr<PropExpr> prop = std::make_unique<OrExpr>(args);
        return make(std::move(prop));
    }

    // TODO: makeAnd() and makeOr() with forwarded args.

    const PropExpr &PropSet::expr(PropRef ref) const {
        if (!ref.isExpr())
            throw std::invalid_argument("Proposition reference should refer to an expression.");
        return *_idToExpr[ref.id()];
    }

    PropRef PropSet::make(std::unique_ptr<PropExpr> prop) {
        auto result = _exprToRef.try_emplace(prop.get(), PROP_EXPR, _idToExpr.size());
        PropRef ref = (*result.first).second;
        if (result.second)
            _idToExpr.push_back(std::move(prop));        
        return ref;
    }
}
