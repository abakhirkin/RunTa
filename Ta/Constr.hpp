#pragma once

#include "Clock.hpp"
#include "Cmp.hpp"

#include <ostream>

namespace Ta {    

    /*! \brief Clock constraint.
    
        A bound on a clock, its negation, or a difference of two clocks.
        The constants true and false are always represented as 0 <= 0 and 0 < 0 respectively.
        The constructor makes sure to not create other representations of true and false (c - c <= 0, 1 < 0, etc). */
    class Constr {
    public:

        /*! \brief creates a constraint.
        
            Creates the constraint \a pos - \a neg \a cmp \a value.
            One or both clocks can be the constant-0-clock.
            When both clocks are the same, and 0 \a cmp \a value, creates the canonical true constraint (0 <= 0).
            When both clocks are the same, and not 0 \a cmp \a value, creates the canonical false constraint (0 < 0). */
        Constr(Clock pos, Clock neg, Cmp cmp, int value);

        /*! \brief Creates the canonical true constraint 0 <= 0 or false constraint 0 < 0. */
        Constr(bool b) : _pos(), _neg(), _cmp(b ? Cmp::LEQ : Cmp::LT), _value() {}

        /*! \brief Clock with coefficient 1. */
        Clock pos() const {
            return _pos;
        }

        /*! \brief Clock with coefficient -1. */
        Clock neg() const {
            return _neg;
        }

        /*! \brief Comparison operator. */
        Cmp cmp() const {
            return _cmp;
        }

        /*! \brief Value of the bound. */
        int value() const {
            return _value;
        }

        /*! \brief Whether the constraint is always true.
        
            Checks equality to the canonical true constraint 0 <= 0. */
        bool isTrue() const {
            return _pos.is0() && _neg.is0() && _cmp == Cmp::LEQ && _value == 0;
        }

        /*! \brief Whether the constraint is always false.
        
            Checks equality to the canonical false constraint 0 <= 0. */
        bool isFalse() const {
            return _pos.is0() && _neg.is0() && _cmp == Cmp::LT && _value == 0;
        }

    private:
        Clock _pos;
        Clock _neg;
        Cmp _cmp;
        int _value;
    };

    /*! \brief Prints the constraint to the stream.

        \relates Constr */
    std::ostream &operator<<(std::ostream &os, Constr c);
}
