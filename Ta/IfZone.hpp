#pragma once

#include "ZoneCommon.hpp"
#include "Constr.hpp"
#include "IfBound.hpp"
#include "PrinterBase.hpp"
#include "CmdList.hpp"
#include "IfInit.hpp"

#include "If/ifzone.h"

#include <ostream>

namespace Ta {

    class IfZone {
    public:
        IfZone(if_type_zone zone) : _zone(zone) {}

        IfZone(TopZoneTag) : _zone(if_universal_zone()) {}

        IfZone(EmptyZoneTag) : _zone(if_empty_zone()) {}

        IfZone(Constr c) : _zone(ofConstr(c)) {}

        IfZone(std::initializer_list<Constr> constraints) : _zone(if_universal_zone()) {
            for (auto c = constraints.begin(), end = constraints.end(); c != end; ++c) {
                _zone = if_intersect_zone(_zone, ofConstr(*c));
            }
        }

        bool isEmpty() const {
            return _zone == if_empty_zone();
        }

        bool isTop() const {
            return _zone == if_universal_zone();
        }

        bool operator==(IfZone other) const {
            return _zone == other._zone;
        }

        bool operator!=(IfZone other) const {
            return _zone != other._zone;
        }

        bool operator<=(IfZone other) const {
            return if_include_zone(_zone, other._zone);
        }

        bool operator>=(IfZone other) const {
            return other <= *this;
        }

        bool operator<(IfZone other) const {
            return (*this <= other) && (*this != other);
        }

        bool operator>(IfZone other) const {
            return (*this >= other) && (*this != other);
        }

        if_type_zone zone() const {
            return _zone;
        }

        void intersect(Constr c) {
            _zone = if_intersect_zone(_zone, ofConstr(c));
        }

        void intersect(IfZone other) {
			if (other._zone != _init.topZone()) {
				_zone = if_intersect_zone(_zone, other._zone);
			}
        }

        void timeElapse() {
            _zone = if_forward_zone(_zone);
        }

        void forget(Clock c) {
            _zone = if_exists_zone(_zone, ifClockId(c));
        }

        void assign(Clock dest, Clock src, int value = 0) {
            if (src.is0()) {
                // Seems that if_set_zone(int, int, int) does not work if the source ('base' in IF's terms)
                // clock is constant 0.
                assign(dest, value);
            }
            else {
                _zone = if_set_zone(_zone, ifClockId(dest), ifClockId(src), value);
            }
        }

        void assign(Clock dest, int value) {
            _zone = if_set_zone(_zone, ifClockId(dest), value);
        }

        void exec(Cmd cmd);

        void exec(const CmdList &commands);

        IfBound bound(Clock pos, Clock neg) const {
            if (!_zone)
                throw std::invalid_argument("Zone is empty.");
            if (pos.is0() && neg.is0())
                return IfBound(if_make_bound(0, IF_SIGN_LE));
            size_t posRow = ifClockRow(pos);
            if (posRow == SIZE_MAX)
                return IfBound(IF_BOUND_INFTY);
            size_t negRow = ifClockRow(neg);
            if (negRow == SIZE_MAX)
                return IfBound(IF_BOUND_INFTY);
            return IfBound(_zone[posRow][negRow]);            
        }

		size_t hash() const {
			return std::hash<if_type_zone>()(_zone);
		}

    private:
        size_t ifClockRow(Clock clock) const {
            if (clock.is0())
                return 0;
            int id = ifClockId(clock);            
            for (int i = 1, size = _zone[0][0]; i <= size; ++i) {
                if (_zone[i][i] == id)
                    return i;
            }
            return SIZE_MAX;
        }

        int ifClockId(Clock c) const {
            return (int)(c.id() + 1);
        }

        if_type_zone ofConstr(Constr c) const {
            return if_clkcmp_zone(ifClockId(c.pos()), ifClockId(c.neg()), c.value(), (int)c.cmp());
        }

        if_type_zone _zone;

		static IfInit _init;
    };

    class IfZonePrinter : public PrinterBase {
    public:
        IfZonePrinter(const IfZone &zone) : _zone(zone) {}

        IfZonePrinter &setNewline(const char *newline) {
            PrinterBase::setNewline(newline);
            return *this;
        }

        const IfZone &zone() const {
            return _zone;
        }

    private:
        const IfZone &_zone;        
    };

    std::ostream &operator<<(std::ostream &os, const IfZonePrinter &printer);

    std::ostream &operator<<(std::ostream &os, const IfZone &zone);
}
