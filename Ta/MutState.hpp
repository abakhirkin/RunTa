#pragma once

#include "Zone.hpp"
#include "PropRef.hpp"

#include <vector>

namespace Ta {

    struct MutTrans;
    struct MutInitTrans;

    struct MutState {
        MutState() : _allowTime(true), _inputInv(PROP_TRUE), _clockInv(ZONE_TOP), _acceptCond(ZONE_EMPTY) {}

        PropRef inputInv() const {
            return _inputInv;
        }

        const Zone &clockInv() const {
            return _clockInv;
        }

        const Zone &acceptCond() const {
            return _acceptCond;
        }

        bool allowTime() const {
            return _allowTime;
        }

        size_t outTransCount() const {
            return outgoing.size();
        }

        MutTrans &outTrans(size_t i) {
            return *outgoing[i];
        }

        const MutTrans &outTrans(size_t i) const {
            return *outgoing[i];
        }

        PropRef _inputInv;
        Zone _clockInv;
        Zone _acceptCond;
        bool _allowTime;
        std::vector<MutTrans*> outgoing;
        std::vector<MutTrans*> incoming;
        std::vector<MutInitTrans*> init;
    };
}
