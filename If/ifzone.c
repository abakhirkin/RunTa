/*
----------------------------------------------------------------------
                I N T E R M E D I A T E    F O R M 
----------------------------------------------------------------------
                Module  : ifzone.C
                Version : 1.20
                Date    : 02/19/01
                Authors : Marius & Jianmin
----------------------------------------------------------------------
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <assert.h>

#include "ifzone.h"


/*
-----------------------------------------------------------------------
Value Implementation
-----------------------------------------------------------------------
*/

void if_print_bound(if_type_bound bound, FILE* file){
  if (bound == IF_BOUND_INFTY)
    fprintf(file, " +oo");
  else
    fprintf(file, "%4d", bound);
}




/*
-----------------------------------------------------------------------
Vector / Vector Table Implementation
-----------------------------------------------------------------------
*/

if_type_vector if_create_vector(unsigned dimension){
  return (if_type_vector) malloc(sizeof(if_type_bound) * dimension);
}

if_type_vector if_duplicate_vector(if_type_vector vector, unsigned dimension){
  if_type_vector result = if_create_vector(dimension);
  memcpy(result, vector, sizeof(if_type_bound) * dimension);
  return result;
}

void if_delete_vector(if_type_vector vector, unsigned){
  free(vector);
}

void if_copy_vector(if_type_vector destination, if_type_vector source, unsigned dimension){
  memcpy(destination, source, sizeof(if_type_bound) * dimension);
}

int if_compare_vector(if_type_vector vector1, if_type_vector vector2, unsigned dimension){
  return memcmp(vector1, vector2, sizeof(if_type_bound) * dimension);
}

unsigned if_hash_vector(if_type_vector vector, unsigned dimension, unsigned base){
  unsigned h = 0;
  for(unsigned i = 0; i < dimension; i++)
    h += (((long) vector[i]) << (3 * i % 17));
  if (h < 0)
    h = -h;
  return h % base;
}

void if_print_vector(if_type_vector vector, unsigned dimension, FILE* file){
  fprintf(file, "[");
  for(unsigned i = 0; i < dimension; i++) {
    if_print_bound(vector[i], file);
    fprintf(file, "%s", i == dimension - 1 ? "]" : " ");
  }
}

/*
-----------------------------------------------------------------------
*/

#define if_vector_table_size 1048573 // A prime close to 2^20

typedef struct if_struct_vector_node {
  unsigned dimension;
  if_type_vector_list list;
  struct if_struct_vector_node * next;
} * if_type_vector_node;

static struct {
  struct {
    struct {
      if_type_vector_node node;
      unsigned count;
    } entry[if_vector_table_size];
    unsigned count;
  } table;
} if_vector;

/*
-----------------------------------------------------------------------
*/

void if_init_vector_table(){
  for(int i = 0; i < if_vector_table_size; i++) {
    if_vector.table.entry[i].node = NULL;
    if_vector.table.entry[i].count = 0;
  }
  if_vector.table.count = 0;  
}

void if_exit_vector_table(){
  for(int i = 0; i < if_vector_table_size; i++) {
    while (if_vector.table.entry[i].node) {
      if_type_vector_node node = if_vector.table.entry[i].node;
      while (node->list) {
	if_type_vector_list list = node->list;
	node->list = list->next;
	free(list->vector);
	free(list);
      }	
      if_vector.table.entry[i].node = node->next;
      free(node);
    }
  }
}

void if_info_vector_table(FILE* file){
  unsigned min, max;
  min = max = if_vector.table.entry[0].count;
  for(int i = 1; i < if_vector_table_size; i++){
    if (min > if_vector.table.entry[i].count)
      min = if_vector.table.entry[i].count;
    if (max < if_vector.table.entry[i].count)
      max = if_vector.table.entry[i].count;
  }
  fprintf(file, "%d vectors stored (%d/max, %d/min, %d/avg)\n",
	  if_vector.table.count, max, min, 
	  if_vector.table.count / if_vector_table_size);
}

if_type_vector if_put_vector(if_type_vector vector, unsigned dimension){
  if_type_vector_node node = NULL;
  if_type_vector_list list = NULL;
  unsigned hash = if_hash_vector(vector, dimension, if_vector_table_size);
  for(node = if_vector.table.entry[hash].node; node != NULL; node = node->next) 
    if (node->dimension == dimension)
      break;
  if (node == NULL) {
    node = (if_type_vector_node) malloc(sizeof(if_struct_vector_node));
    node->dimension = dimension;
    node->list = NULL;
    node->next = if_vector.table.entry[hash].node;
    if_vector.table.entry[hash].node = node;
  }
  for(list = node->list; list != NULL; list = list->next)
    if (! if_compare_vector(list->vector, vector, dimension))
      break;
  if (list == NULL) {
    list = (if_type_vector_list) malloc(sizeof(if_struct_vector_list));
    list->vector = if_duplicate_vector(vector, dimension);
    list->next = node->list;
    node->list = list;
    if_vector.table.entry[hash].count++;
    if_vector.table.count++;    
  }
  return list->vector;
}




/*
-----------------------------------------------------------------------
Matrix / Matrix Table Implementation
-----------------------------------------------------------------------
*/

if_type_matrix if_create_matrix(unsigned dimension){
  return (if_type_matrix) malloc(sizeof(if_type_vector) * dimension);
}

if_type_matrix if_duplicate_matrix(if_type_matrix matrix, unsigned dimension){
  if_type_matrix result = if_create_matrix(dimension);
  memcpy(result, matrix, sizeof(if_type_vector) * dimension);
  return result;
}

void if_delete_matrix(if_type_matrix matrix, unsigned dimension){
  free(matrix);
}

void if_copy_matrix(if_type_matrix destination, if_type_matrix source, unsigned dimension){
  memcpy(destination, source, sizeof(if_type_vector) * dimension);
}

int if_compare_matrix(if_type_matrix matrix1, if_type_matrix matrix2, unsigned dimension){
  return memcmp(matrix1, matrix2, sizeof(if_type_vector) * dimension);
}

unsigned if_hash_matrix(if_type_matrix matrix, unsigned dimension, unsigned base){
  unsigned h = 0;
  for(unsigned i = 0; i < dimension; i++)
    h += ((((long) matrix[i]) >> 4) << (3 * i  % 17));
  if (h < 0)
    h = -h;
  return h % base;
}

void if_print_matrix(if_type_matrix matrix, unsigned dimension, FILE* file){
  for(unsigned i = 0; i < dimension; i++){
    if_print_vector(matrix[i], dimension, file);
    fprintf(file, "\n");
  }
}

/*
-----------------------------------------------------------------------
*/

#define if_matrix_table_size 1048573 // A prime close to 2^20

#define if_matrix_max_dimension 64

typedef struct if_struct_matrix_node {
  unsigned dimension;
  if_type_matrix_list list;
  struct if_struct_matrix_node * next;
} * if_type_matrix_node;

static struct {
  struct {
    struct {
      if_type_matrix_node node;
      unsigned count;
    } entry[if_matrix_table_size];
    unsigned count;
  } table;
  struct {
    if_type_matrix lines;
    if_type_matrix matrix;
  } cache;
  if_type_matrix extrapolation;
} if_matrix;

/*
-----------------------------------------------------------------------
*/

void if_init_matrix_table(){
  for(int i = 0; i < if_matrix_table_size; i++) {
    if_matrix.table.entry[i].node = NULL;
    if_matrix.table.entry[i].count = 0;
  }
  if_matrix.table.count = 0;
  if_matrix.cache.lines = if_create_matrix(if_matrix_max_dimension);
  for(int i = 0; i < if_matrix_max_dimension; i++)
    if_matrix.cache.lines[i] = if_create_vector(if_matrix_max_dimension);
  if_matrix.cache.matrix = if_create_matrix(if_matrix_max_dimension);
  if_matrix.extrapolation = if_universal_zone();
}

void if_exit_matrix_table(){
  for(int i = 0; i < if_matrix_table_size; i++) {
    while (if_matrix.table.entry[i].node) {
      if_type_matrix_node node = if_matrix.table.entry[i].node;
      while (node->list) {
	if_type_matrix_list list = node->list;
	node->list = list->next;
	free(list->matrix);
	free(list);
      }	
      if_matrix.table.entry[i].node = node->next;
      free(node);
    }
  }
  for(int i = 0; i < if_matrix_max_dimension; i++)
    if_delete_vector(if_matrix.cache.lines[i], if_matrix_max_dimension);
  if_delete_matrix(if_matrix.cache.lines, if_matrix_max_dimension);
  if_delete_matrix(if_matrix.cache.matrix, if_matrix_max_dimension);
}

void if_info_matrix_table(FILE* file){
  unsigned min, max;
  min = max = if_matrix.table.entry[0].count;
  for(int i = 1; i < if_matrix_table_size; i++){
    if (min > if_matrix.table.entry[i].count)
      min = if_matrix.table.entry[i].count;
    if (max < if_matrix.table.entry[i].count)
      max = if_matrix.table.entry[i].count;
  }
  fprintf(file, "%d matrixes stored (%d/max, %d/min, %d/avg)\n",
	  if_matrix.table.count, max, min, 
	  if_matrix.table.count / if_matrix_table_size);
}

if_type_matrix if_put_matrix(if_type_matrix matrix, unsigned dimension){
  if_type_matrix_node node = NULL;
  if_type_matrix_list list = NULL;
  unsigned hash = if_hash_matrix(matrix, dimension, if_matrix_table_size);
  for(node = if_matrix.table.entry[hash].node; node != NULL; node = node->next) 
    if (node->dimension == dimension)
      break;
  if (node == NULL) {
    node = (if_type_matrix_node) malloc(sizeof(if_struct_matrix_node));
    node->dimension = dimension;
    node->list = NULL;
    node->next = if_matrix.table.entry[hash].node;
    if_matrix.table.entry[hash].node = node;
  }
  for(list = node->list; list != NULL; list = list->next)
    if (! if_compare_matrix(list->matrix, matrix, dimension))
      break;
  if (list == NULL) {
    list = (if_type_matrix_list) malloc(sizeof(if_struct_matrix_list));
    list->matrix = if_duplicate_matrix(matrix, dimension);
    list->next = node->list;
    node->list = list;
    if_matrix.table.entry[hash].count++;
    if_matrix.table.count++;    
  }
  return list->matrix;
}




/*
-----------------------------------------------------------------------
Zone Implementation
-----------------------------------------------------------------------
*/


if_type_zone if_empty_zone(){
  return NULL;
}

if_type_zone if_universal_zone(){
  if_type_zone result;
  if_matrix.cache.lines[0][0] = 0;
  if_matrix.cache.matrix[0] = if_put_vector(if_matrix.cache.lines[0], 1);
  result = if_put_matrix(if_matrix.cache.matrix, 1);
  return result;
}

int if_is_empty_zone(if_type_zone zone){
  return zone == NULL;
}

int if_clock_in_zone(if_type_zone zone, int clock) {
  int in = 0;
  if (!zone)
    return 0;
  for(int k = 1; k <= zone[0][0] && !in; k++)
    if (clock == zone[k][k])
      in = 1;
  return in;
}

/*
-----------------------------------------------------------------------
*/

if_type_zone if_clkcmp_zone(int clock1, int clock2, int value, int sign) {
  if_type_zone result;
  if (clock1 == clock2)
    result = (sign == IF_SIGN_LT && 0 <  value ||
	      sign == IF_SIGN_LE && 0 <= value) ? 
      if_universal_zone() : if_empty_zone();
  else {
    if_type_bound bound = if_make_bound(value,sign); 
    if (clock1 == 0 || clock2 == 0) {
      if_matrix.cache.lines[0][0] = 1;
      if_matrix.cache.lines[0][1] = clock1 == 0 ? bound : IF_BOUND_INFTY;
      if_matrix.cache.lines[1][0] = clock2 == 0 ? bound : IF_BOUND_INFTY;
      if_matrix.cache.lines[1][1] = clock1 ? clock1 : clock2;
      if_matrix.cache.matrix[0] = if_put_vector(if_matrix.cache.lines[0], 2);
      if_matrix.cache.matrix[1] = if_put_vector(if_matrix.cache.lines[1], 2);
      result = if_put_matrix(if_matrix.cache.matrix, 2);
    }
    else {
      if_matrix.cache.lines[0][0] = 2;
      if_matrix.cache.lines[0][1] = IF_BOUND_INFTY;
      if_matrix.cache.lines[0][2] = IF_BOUND_INFTY;
      if_matrix.cache.lines[1][0] = IF_BOUND_INFTY;
      if_matrix.cache.lines[1][1] = clock1 < clock2 ? clock1 : clock2;
      if_matrix.cache.lines[1][2] = clock1 < clock2 ? bound : IF_BOUND_INFTY;
      if_matrix.cache.lines[2][0] = IF_BOUND_INFTY;
      if_matrix.cache.lines[2][1] = clock2 < clock1 ? bound : IF_BOUND_INFTY;
      if_matrix.cache.lines[2][2] = clock2 < clock1 ? clock1 : clock2;
      if_matrix.cache.matrix[0] = if_put_vector(if_matrix.cache.lines[0], 3);
      if_matrix.cache.matrix[1] = if_put_vector(if_matrix.cache.lines[1], 3);
      if_matrix.cache.matrix[2] = if_put_vector(if_matrix.cache.lines[2], 3);
      result = if_put_matrix(if_matrix.cache.matrix, 3);
    }
  }
  return result;
}

if_type_zone if_clklt_zone(int clock1, int clock2, int value){
  return if_clkcmp_zone(clock1, clock2, value, IF_SIGN_LT);
}

if_type_zone if_clkle_zone(int clock1, int clock2, int value){
  return if_clkcmp_zone(clock1, clock2, value, IF_SIGN_LE);
}

if_type_zone if_clkge_zone(int clock1, int clock2, int value){
  return if_clkle_zone(clock2, clock1, -value);
}

if_type_zone if_clkgt_zone(int clock1, int clock2, int value){
  return if_clklt_zone(clock2, clock1, -value);
}

if_type_zone if_clkeq_zone(int clock1, int clock2, int value){
  return if_intersect_zone(if_clkle_zone(clock1, clock2, value),
			   if_clkle_zone(clock2, clock1, -value));
}

/*
-----------------------------------------------------------------------
*/

int if_normalize_zone(if_type_zone zone){

  for(int k = 0; k <= zone[0][0]; k++)
    for(int i = 0; i <= zone[0][0]; i++) 
      if (i != k && zone[i][k] < IF_BOUND_INFTY) { 

	if (zone[k][i] < IF_BOUND_INFTY && 
	    if_add_bound(zone[i][k],zone[k][i]) < if_make_bound(0,IF_SIGN_LE))
	  return 0;

	for (int j = 0; j <= zone[0][0]; j++)
	  if (i != j && j != k && zone[k][j] < IF_BOUND_INFTY &&
	      if_add_bound(zone[i][k], zone[k][j]) < zone[i][j] )
	    zone[i][j] = if_add_bound(zone[i][k], zone[k][j]);
	
      }
  return 1;

}

/*
-----------------------------------------------------------------------
*/

int if_extrapolate_zone(if_type_zone zone, if_type_zone ezone){

  for(int i = 0, ie = 0; i <= zone[0][0]; i++, ie++) {
    for(; i != 0 && ie <= ezone[0][0] && ezone[ie][ie] < zone[i][i]; ie++);
    if (i == 0 || (ie <= ezone[0][0] && ezone[ie][ie] == zone[i][i])) 
      for(int j = 0, je = 0; j <= zone[0][0]; j++, je++) {
	for(; j != 0 && je <= ezone[0][0] && ezone[je][je] < zone[j][j]; je++);
	if (j == 0 || (je <= ezone[0][0] && ezone[je][je] == zone[j][j])) 
	  if (i != j && zone[i][j] < IF_BOUND_INFTY){

	    if (ezone[ie][je] < IF_BOUND_INFTY && 
		zone[i][j] > ezone[ie][je]) {
	      zone[i][j] = IF_BOUND_INFTY;
	      continue;
	    }
	    if (ezone[je][ie] < IF_BOUND_INFTY &&
		zone[i][j] < if_complement_bound(ezone[je][ie])) {
	      zone[i][j] = if_complement_bound(ezone[je][ie]);
	      continue;
	    }

	  }
      }
  }
  return 1;

}

/*
-----------------------------------------------------------------------
*/

if_type_zone if_intersect_zone(if_type_zone zone1, if_type_zone zone2){
  unsigned dimension1, dimension2, dimension, k;

  if (if_is_empty_zone(zone1) || if_is_empty_zone(zone2))
    return if_empty_zone();

  dimension1 = zone1[0][0] + 1;
  dimension2 = zone2[0][0] + 1;
  k = 1;
  for(unsigned i1 = 1, i2 = 1; i1 < dimension1 || i2 < dimension2;) {
    for(; i1 < dimension1 && (i2 == dimension2 || 
			      zone1[i1][i1] < zone2[i2][i2]); i1++, k++)
      if_matrix.cache.lines[k][k] = zone1[i1][i1];
    for(; i2 < dimension2 && (i1 == dimension1 ||
			      zone2[i2][i2] < zone1[i1][i1]); i2++, k++)
      if_matrix.cache.lines[k][k] = zone2[i2][i2];
    for(; i1 < dimension1 && (i2 < dimension2 && 
			      zone1[i1][i1] == zone2[i2][i2]); i1++, i2++, k++)
      if_matrix.cache.lines[k][k] = zone1[i1][i1];
  }
  
  dimension = k;
  if_matrix.cache.lines[0][0] = dimension - 1;
  
  for(unsigned i = 0; i < dimension; i++)
    for(unsigned j = 0; j < dimension; j++)
      if (i != j)
	if_matrix.cache.lines[i][j] = IF_BOUND_INFTY;
  
  for(unsigned i1 = 0, i = 0; i1 < dimension1; i1++, i++) {
    for(; i1 != 0 && if_matrix.cache.lines[i][i] != zone1[i1][i1]; i++);
    for(unsigned j1 = 0, j = 0; j1 < dimension1; j1++, j++) {
      for(; j1 != 0 && if_matrix.cache.lines[j][j] != zone1[j1][j1]; j++);
      if (i1 != j1)
	if_matrix.cache.lines[i][j] = zone1[i1][j1];
    }
  }
  
  for(unsigned i2 = 0, i = 0; i2 < dimension2; i2++, i++) {
    for(; i2 != 0 && if_matrix.cache.lines[i][i] != zone2[i2][i2]; i++);
    for(unsigned j2 = 0, j = 0; j2 < dimension2; j2++, j++) {
      for(; j2 != 0 && if_matrix.cache.lines[j][j] != zone2[j2][j2]; j++);
      if (i2 != j2) 
	if_matrix.cache.lines[i][j] = 
	  if_min_bound(if_matrix.cache.lines[i][j],zone2[i2][j2]);
    }
  }
  
  if (!if_normalize_zone(if_matrix.cache.lines))
    return if_empty_zone();
  
  if_extrapolate_zone(if_matrix.cache.lines, if_matrix.extrapolation);
  
  for(unsigned i = 0; i < dimension; i++) 
    if_matrix.cache.matrix[i] = if_put_vector(if_matrix.cache.lines[i], dimension);
  return if_put_matrix(if_matrix.cache.matrix, dimension);

}

/*
-----------------------------------------------------------------------
*/

if_type_zone if_exists_zone(if_type_zone zone, int clock){
  unsigned dimension, k;

  if (if_is_empty_zone(zone))
    return zone;

  dimension = zone[0][0] + 1;
  for(k = 1; k < dimension; k++)
    if (zone[k][k] == clock)
      break;
  if (k == dimension) 
    return zone;

  for(unsigned i = 0; i < k; i++) {
    for(unsigned j = 0; j < k; j++)
      if_matrix.cache.lines[i][j] = zone[i][j];
    for(unsigned j = k + 1; j < dimension; j++)
      if_matrix.cache.lines[i][j-1] = zone[i][j];
  }
  for(unsigned i = k + 1; i < dimension; i++) {
    for(unsigned j = 0; j < k; j++)
      if_matrix.cache.lines[i-1][j] = zone[i][j];
    for(unsigned j = k + 1; j < dimension; j++)
      if_matrix.cache.lines[i-1][j-1] = zone[i][j];
  }
  if_matrix.cache.lines[0][0]--;
  dimension--;

  for(unsigned i = 0; i < dimension; i++)
    if_matrix.cache.matrix[i] = if_put_vector(if_matrix.cache.lines[i], dimension);
  return if_put_matrix(if_matrix.cache.matrix, dimension);

}

/*
-----------------------------------------------------------------------
*/

if_type_zone if_set_zone(if_type_zone zone, int clock, int value){
  unsigned dimension, k;

  if (if_is_empty_zone(zone))
    return zone;

  dimension = zone[0][0] + 1;
  for(k = 1; k < dimension; k++)
    if (zone[k][k] == clock)
      break;
  if (k == dimension)
    return if_intersect_zone(zone, if_clkeq_zone(clock, 0, value));

  for(unsigned i = 0; i < dimension; i++)
    for(unsigned j = 0; j < dimension; j++)
      if_matrix.cache.lines[i][j] = zone[i][j];
  for(unsigned i = 0; i < dimension; i++)
    if (i != k)
      if_matrix.cache.lines[i][k] = if_matrix.cache.lines[k][i] = IF_BOUND_INFTY;
  if_matrix.cache.lines[k][0] = if_make_bound(value, IF_SIGN_LE);
  if_matrix.cache.lines[0][k] = if_make_bound(-value, IF_SIGN_LE);

  if (!if_normalize_zone(if_matrix.cache.lines))
    return if_empty_zone();

  if_extrapolate_zone(if_matrix.cache.lines, if_matrix.extrapolation);

  for(unsigned i = 0; i < dimension; i++)
    if_matrix.cache.matrix[i] = if_put_vector(if_matrix.cache.lines[i], dimension);
  return if_put_matrix(if_matrix.cache.matrix, dimension);
  
}

if_type_zone if_set_zone(if_type_zone zone, int clock, int baseClock, int value) {
  unsigned dimension, k, bk;

  if (if_is_empty_zone(zone))
    return zone;

  dimension = zone[0][0] + 1;
  for(bk = 1; bk < dimension; bk++)
    if (zone[bk][bk] == baseClock)
      break;
  assert(bk != dimension);

  for(k = 1; k < dimension; k++)
    if (zone[k][k] == clock)
      break;
  if (k == dimension)
    return if_intersect_zone(zone, if_clkeq_zone(clock, baseClock, value));

  assert(bk != k); 

  for(unsigned i = 0; i < dimension; i++)
    for(unsigned j = 0; j < dimension; j++)
      if_matrix.cache.lines[i][j] = zone[i][j];
  for(unsigned i = 0; i < dimension; i++)
    if (i != k) 
      if_matrix.cache.lines[i][k] = if_matrix.cache.lines[k][i] = IF_BOUND_INFTY;
  if_matrix.cache.lines[k][bk] = if_make_bound(value, IF_SIGN_LE);
  if_matrix.cache.lines[bk][k] = if_make_bound(-value, IF_SIGN_LE);

  if (!if_normalize_zone(if_matrix.cache.lines))
    return if_empty_zone();

  if_extrapolate_zone(if_matrix.cache.lines, if_matrix.extrapolation);

  for(unsigned i = 0; i < dimension; i++)
    if_matrix.cache.matrix[i] = if_put_vector(if_matrix.cache.lines[i], dimension);
  return if_put_matrix(if_matrix.cache.matrix, dimension);
  
}


/*
-----------------------------------------------------------------------
*/

if_type_zone if_round_zone(if_type_zone zone, int direction, int sign) {
  unsigned dimension, rounded = 1;
  
  if (if_is_empty_zone(zone))
    return zone;

  dimension = zone[0][0] + 1;
  for(unsigned i = 1; i < dimension && rounded; i++) {
    if ((direction & 1) &&  // forward / right
	zone[i][0] < IF_BOUND_INFTY && 
	if_sign_bound(zone[i][0]) != sign)
      rounded = 0; 
    if ((direction & 2) &&  // backward / left
	zone[0][i] < IF_BOUND_INFTY &&
	if_sign_bound(zone[0][i]) != sign)
      rounded = 0;
  }
  
  if (rounded)
    return zone;

  for(unsigned i = 0; i < dimension; i++)
    for(unsigned j = 0; j < dimension; j++)
      if_matrix.cache.lines[i][j] = zone[i][j];
  for(unsigned i = 1; i < dimension; i++) {
    if ((direction & 1) && 
	zone[i][0] < IF_BOUND_INFTY)
      if_matrix.cache.lines[i][0] = 
	if_make_bound(if_value_bound(zone[i][0]), sign);
    if ((direction & 2) && 
	zone[0][i] < IF_BOUND_INFTY)
      if_matrix.cache.lines[0][i] = 
	if_make_bound(if_value_bound(zone[0][i]), sign);
    if (sign == IF_SIGN_LE) 
      for(unsigned i = 1; i < dimension; i++)
	for(unsigned j = 1; j < dimension; j++)
	  if (i != j && zone[i][j] < IF_BOUND_INFTY)
	    if_matrix.cache.lines[i][j] =
	      if_make_bound(if_value_bound(zone[i][j]), sign);
  }

  if (!if_normalize_zone(if_matrix.cache.lines))
    return if_empty_zone();
  
  if_extrapolate_zone(if_matrix.cache.lines, if_matrix.extrapolation);

  for(unsigned i = 0; i < dimension; i++)
    if_matrix.cache.matrix[i] = if_put_vector(if_matrix.cache.lines[i], dimension);
  return if_put_matrix(if_matrix.cache.matrix, dimension);
  
}

/* * */

if_type_zone if_open_right_zone(if_type_zone zone) {
  return if_round_zone(zone, 1, IF_SIGN_LT);
}

if_type_zone if_close_right_zone(if_type_zone zone) {
  return if_round_zone(zone, 1, IF_SIGN_LE);
}

if_type_zone if_open_left_zone(if_type_zone zone) {
  return if_round_zone(zone, 2, IF_SIGN_LT);
}

if_type_zone if_close_left_zone(if_type_zone zone) {
  return if_round_zone(zone, 2, IF_SIGN_LE);
}

/* * */

if_type_zone if_open_zone(if_type_zone zone) {
  return if_open_right_zone(zone);
}

if_type_zone if_close_zone(if_type_zone zone) {
  return if_close_right_zone(zone);
}


/*
-----------------------------------------------------------------------
*/

if_type_zone if_backward_zone(if_type_zone zone){
  unsigned dimension, backward = 1;

  if (if_is_empty_zone(zone))
    return zone;
 
  dimension = zone[0][0] + 1;
  for(unsigned i = 1; i < dimension && backward; i++)
    if (zone[0][i] < IF_BOUND_INFTY)
      backward = 0;
  if (backward)
    return zone;

  for(unsigned i = 0; i < dimension; i++)
    for(unsigned j = 0; j < dimension; j++)
      if_matrix.cache.lines[i][j] = zone[i][j];
  for(unsigned i = 1; i < dimension; i++)
    if_matrix.cache.lines[0][i] = IF_BOUND_INFTY;

  if (!if_normalize_zone(if_matrix.cache.lines))
    return if_empty_zone();
  
  if_extrapolate_zone(if_matrix.cache.lines, if_matrix.extrapolation);

  for(unsigned i = 0; i < dimension; i++)
    if_matrix.cache.matrix[i] = if_put_vector(if_matrix.cache.lines[i], dimension);
  return if_put_matrix(if_matrix.cache.matrix, dimension);

}

/*
-----------------------------------------------------------------------
*/

if_type_zone if_forward_zone(if_type_zone zone){
  unsigned dimension, forward = 1;

  if (if_is_empty_zone(zone))
    return zone;
 
  dimension = zone[0][0] + 1;
  for(unsigned i = 1; i < dimension && forward; i++)
    if (zone[i][0] < IF_BOUND_INFTY)
      forward = 0;
  if (forward)
    return zone;

  for(unsigned i = 0; i < dimension; i++)
    for(unsigned j = 0; j < dimension; j++)
      if_matrix.cache.lines[i][j] = zone[i][j];
  for(unsigned i = 1; i < dimension; i++)
    if_matrix.cache.lines[i][0] = IF_BOUND_INFTY;

  if (!if_normalize_zone(if_matrix.cache.lines))
    return if_empty_zone();
  
  if_extrapolate_zone(if_matrix.cache.lines, if_matrix.extrapolation);

  for(unsigned i = 0; i < dimension; i++)
    if_matrix.cache.matrix[i] = if_put_vector(if_matrix.cache.lines[i], dimension);
  return if_put_matrix(if_matrix.cache.matrix, dimension);

}

/*
-----------------------------------------------------------------------
*/

if_type_zone if_since_zone(if_type_zone zone, if_type_zone tpc){
  return if_close_zone
    (if_intersect_zone
     (tpc, if_forward_zone(if_intersect_zone(zone,tpc))));
}

if_type_zone if_until_zone(if_type_zone zone, if_type_zone tpc) {
  return if_intersect_zone
    (zone, if_backward_zone(if_intersect_zone(if_close_zone(zone), 
					      if_close_zone(tpc))));
}

/*
-----------------------------------------------------------------------
*/

if_type_zone if_diamond_zone(if_type_zone zone, int k){
  unsigned dimension;

  if (if_is_empty_zone(zone))
    return zone;

  if (k == 0)
    return zone;

  dimension = zone[0][0] + 1;
  for(unsigned i = 0; i < dimension; i++)
    for(unsigned j = 0; j < dimension; j++)
      if_matrix.cache.lines[i][j] = zone[i][j];
  for(unsigned i = 1; i < dimension; i++)
    if (if_matrix.cache.lines[0][i] < IF_BOUND_INFTY)
      if_matrix.cache.lines[0][i] += k;

  if (!if_normalize_zone(if_matrix.cache.lines))
    return if_empty_zone();
  
  if_extrapolate_zone(if_matrix.cache.lines, if_matrix.extrapolation);
  
  for(unsigned i = 0; i < dimension; i++) 
    if_matrix.cache.matrix[i] = if_put_vector(if_matrix.cache.lines[i], dimension);
  return if_put_matrix(if_matrix.cache.matrix, dimension);
  
}

/*
-----------------------------------------------------------------------
*/

if_type_zone if_convex_hull_zone(if_type_zone zone1, if_type_zone zone2){
  unsigned dimension1, dimension2, dimension, k;
  
  dimension1 = zone1[0][0] + 1;
  dimension2 = zone2[0][0] + 1;
  k = 1;
  
  for(unsigned i1 = 1, i2 = 1; i1 < dimension1 && i2 < dimension2;) {
    if (zone1[i1][i1] <  zone2[i2][i2]) { i1++; continue; }
    if (zone1[i1][i1] >  zone2[i2][i2]) { i2++; continue; }
    if (zone1[i1][i1] == zone2[i2][i2]) {
      if_matrix.cache.lines[k][k] = zone1[i1][i1];
      k++; i1++; i2++;
    }
  }
  
  dimension = k;
  if_matrix.cache.lines[0][0] = dimension - 1;
  
  for(unsigned i = 0; i < dimension; i++)
    for(unsigned j = 0; j < dimension; j++)
      if (i != j)
	if_matrix.cache.lines[i][j] = IF_BOUND_INFTY;


  for(unsigned i = 0, i1 = 0; i < dimension; i++, i1++) {
    for(; i != 0 && if_matrix.cache.lines[i][i] != zone1[i1][i1]; i1++);
    for(unsigned j = 0, j1 = 0; j < dimension; j++, j1++) {
      for(; j != 0 && if_matrix.cache.lines[j][j] != zone1[j1][j1]; j1++);
      if (i != j)
	if_matrix.cache.lines[i][j] = zone1[i1][j1];
    }
  }

  for(unsigned i = 0, i2 = 0; i < dimension; i++, i2++) {
    for(; i != 0 && if_matrix.cache.lines[i][i] != zone2[i2][i2]; i2++);
    for(unsigned j = 0, j2 = 0; j < dimension; j++, j2++) {
      for(; j != 0 && if_matrix.cache.lines[j][j] != zone2[j2][j2]; j2++);
      if (i != j)
	if_matrix.cache.lines[i][j] = 
	  if_max_bound(if_matrix.cache.lines[i][j], zone2[i2][j2]);
    }
  }
  
  if (!if_normalize_zone(if_matrix.cache.lines))
    return if_empty_zone();
  
  if_extrapolate_zone(if_matrix.cache.lines, if_matrix.extrapolation);
  
  for(unsigned i = 0; i < dimension; i++) 
    if_matrix.cache.matrix[i] = if_put_vector(if_matrix.cache.lines[i], dimension);
  return if_put_matrix(if_matrix.cache.matrix, dimension);

}


/*
-----------------------------------------------------------------------
*/

int if_include_zone(if_type_zone zone1, if_type_zone zone2){
  unsigned dimension1, dimension2;

  if (if_is_empty_zone(zone1))
    return 1;
  if (if_is_empty_zone(zone2))
    return 0;

  dimension1 = zone1[0][0] + 1;
  dimension2 = zone2[0][0] + 1;

  if (dimension1 < dimension2)
    return 0;

  for(unsigned i2 = 1, i1 = 1; i2 < dimension2; i2++, i1++) {
    for(; i1 < dimension1 && zone1[i1][i1] < zone2[i2][i2]; i1++);
    if (i1 == dimension1 || zone1[i1][i1] > zone2[i2][i2])
      return 0;
  }

  for(unsigned i2 = 0, i1 = 0; i2 < dimension2; i2++, i1++){
    for(; i2 && i1 < dimension1 && zone1[i1][i1] < zone2[i2][i2]; i1++);
    for(unsigned j2 = 0, j1 = 0; j2 < dimension2; j2++, j1++){
      for(; j2 && j1 < dimension1 && zone1[j1][j1] < zone2[j2][j2]; j1++);
      if (i2 != j2 && zone1[i1][j1] > zone2[i2][j2])
	return 0;
    }
  }
  
  return 1;
}

/*
-----------------------------------------------------------------------
*/

void if_print_matrix_zone(if_type_zone zone, FILE* file){
  fprintf(file,"\n");
  if (zone)
    if_print_matrix(zone, zone[0][0] + 1, file);
  else
    fprintf(file, "[]\n");
}

void if_print_zone(if_type_zone zone, FILE* file){
  unsigned dimension, universal = 1;

  if (!zone) {
    fprintf(file, " false");
    return;
  }
  
  dimension = zone[0][0] + 1;
  for(unsigned i = 0; i < dimension; i++)
    for(unsigned j = 0; j < i; j++){
      unsigned rok = 0, lok = 0;
      if (zone[i][j] < IF_BOUND_INFTY)
	rok = 1;
      if (zone[j][i] < IF_BOUND_INFTY)
	lok = 1;
      if (lok || rok) {
	universal = 0;
	fprintf(file, " c%d", zone[i][i]);
	if (j)
	  fprintf(file, "-c%d", zone[j][j]);
	fprintf(file, "=");
	if (lok)
	  fprintf(file, "%s%d", if_sign_bound(zone[j][i]) ? "[" : "(",
		  -if_value_bound(zone[j][i]));
	else
	  fprintf(file, "(_");
	fprintf(file, ",");
	if (rok)
	  fprintf(file, "%d%s", if_value_bound(zone[i][j]),
		  if_sign_bound(zone[i][j]) ? "]" : ")");
	else
	  fprintf(file, "_)");
      }
    }

  if (universal)
    fprintf(file, " true");

}

unsigned if_hash_zone(if_type_zone zone, unsigned base) {
  if (!zone) 
    return 0;

  return if_hash_matrix(zone, zone[0][0]+1, base);
}


/*
-----------------------------------------------------------------------
Zone Extrapolation
-----------------------------------------------------------------------
*/

void if_restrict_extrapolation_zone(if_type_zone zone){
  if_matrix.extrapolation = if_intersect_zone(if_matrix.extrapolation, zone);
}

void if_restrict_extrapolation_zone(int l) {
  for(int i = 1; i < if_matrix_max_dimension; i++) {
    if_type_zone z = if_intersect_zone(if_clkle_zone(i, 0, l),
				       if_clkge_zone(i, 0, 0));
    if_restrict_extrapolation_zone(z);
  }
}




/*
-----------------------------------------------------------------------
Zone List 
-----------------------------------------------------------------------
*/

void if_init_zone_list(if_type_zone_list* zl, if_type_zone z) {
  (*zl) = (if_type_zone_list) malloc(sizeof(struct if_struct_zone_list));
  (*zl)->zone = z;
  (*zl)->next = NULL;
}

void if_insert_zone_list(if_type_zone_list* zl, if_type_zone z) {
  if_type_zone_list l = (if_type_zone_list) malloc(sizeof(struct if_struct_zone_list));
  l->zone = z;
  l->next = (*zl);
  (*zl) = l;
}

void if_clear_zone_list(if_type_zone_list* zl) {
  while ((*zl) != NULL) {
    if_type_zone_list head = (*zl);
    (*zl) = (*zl)->next;
    free(head);
  }
}

/*
-----------------------------------------------------------------------
*/

void if_intersect_zone_list(if_type_zone_list* zl, if_type_zone z){
  if_type_zone_list result = NULL;
  for(if_type_zone_list l = (*zl); l != NULL; l = l->next) {
    if_type_zone l_z = if_intersect_zone(l->zone, z);
    if ( !if_is_empty_zone(l_z) ) 
      if_insert_zone_list(&result, l_z);
  }
  if_clear_zone_list(zl);
  (*zl) = result;
}

void if_intersect_zone_list(if_type_zone_list* zl, if_type_zone_list zj) {
  if_type_zone_list result = NULL;
  for(if_type_zone_list l = (*zl); l != NULL; l = l->next) 
    for(if_type_zone_list j = zj; j != NULL; j = j->next) {
      if_type_zone l_j = if_intersect_zone(l->zone, j->zone);
      if ( !if_is_empty_zone(l_j) ) 
	if_insert_zone_list(&result, l_j);
    }
  if_clear_zone_list(zl);
  (*zl) = result;
}

/*
-----------------------------------------------------------------------
*/

void if_guard_tpc_zone_list(if_type_zone_list* zl, if_type_zone guard, int deadline){
  assert (!if_is_empty_zone(guard));
  assert (deadline == 0 || deadline == 1); 

  if_type_zone_list tpclist = NULL; // time progress zone list induced by the guard

  if (deadline == 1) { // delayable
    guard = if_backward_zone(guard);

    if_type_zone tpc = if_open_zone(guard);
    if (tpc == guard)
      return;

    if_insert_zone_list(&tpclist, tpc);
  }

  if (deadline == 0 || deadline == 1) { // delayable or eager
    unsigned dimension = guard[0][0] + 1;
    unsigned universal = 1;
    
    for(unsigned i = 0; i < dimension && universal; i++)
      for(unsigned j = 0; j < dimension && universal; j++) 
	if (i != j && guard[i][j] < IF_BOUND_INFTY)
	  universal = 0;
    
    if (!universal) {
      for(unsigned i = 0; i < dimension; i++)
	for(unsigned j = 0; j < dimension; j++)
	  if (i != j && guard[i][j] < IF_BOUND_INFTY &&
	      (i == 0 || j == 0 || 
	       guard[i][0] == IF_BOUND_INFTY || guard[0][j] == IF_BOUND_INFTY ||
	       guard[i][j] < if_add_bound(guard[i][0], guard[0][j]))) {
	    if_type_zone tpc = if_clkcmp_zone(j ? guard[j][j] : 0, 
					      i ? guard[i][i] : 0, 
					     -if_value_bound(guard[i][j]), 
					     !if_sign_bound(guard[i][j]));
	    if_insert_zone_list(&tpclist, tpc);
	  }
    }
    
    if_intersect_zone_list(zl, tpclist);

    if_clear_zone_list(&tpclist);
  }
  
}

/*
-----------------------------------------------------------------------
*/

void if_guard_split_zone_list(if_type_zone_list* zl, if_type_zone guard) {
  assert(!if_is_empty_zone(guard));

  if_type_zone_list split; // full space partition according to guard and its negation
  if_init_zone_list(&split, guard);

  if_type_zone current = if_universal_zone();
  unsigned dimension = guard[0][0] + 1;

  for(unsigned i = 0; i < dimension; i++)
    for(unsigned j = 0; j < dimension; j++) {
      if (i == j || guard[i][j] == IF_BOUND_INFTY) 
	continue; // skip diagonal and unconstrained

      if (i != 0 && j != 0 && guard[i][0] < IF_BOUND_INFTY && guard[0][j] < IF_BOUND_INFTY &&
	  guard[i][j] == if_add_bound(guard[i][0], guard[0][j]))
	continue; // skip redundant (small trick)

      if_type_zone neg_ij = if_clkcmp_zone(j ? guard[j][j] : 0,
					   i ? guard[i][i] : 0,
					  -if_value_bound(guard[i][j]),
					  !if_sign_bound(guard[i][j]));
      if_type_zone pos_ij = if_clkcmp_zone(i ? guard[i][i] : 0,
					   j ? guard[j][j] : 0,
					   if_value_bound(guard[i][j]),
					   if_sign_bound(guard[i][j]));
      
      if_type_zone gs = if_intersect_zone(current, neg_ij);
      if (! if_is_empty_zone(gs) )
	if_insert_zone_list(&split, gs);
      
      current = if_intersect_zone(current, pos_ij);      
    }

  if_intersect_zone_list(zl, split);
  
  if_clear_zone_list(&split);  
}

/*
-----------------------------------------------------------------------
Init / Exit Zone Module
-----------------------------------------------------------------------
*/

void if_init_zone_module() {
  if_init_vector_table();
  if_init_matrix_table();
}

void if_exit_zone_module() {
  // if_info_matrix_table(stdout);
  if_exit_matrix_table();
  // if_info_vector_table(stdout);
  if_exit_vector_table();
}

/*
-----------------------------------------------------------------------
*/

