/*
----------------------------------------------------------------------
                I N T E R M E D I A T E    F O R M 
----------------------------------------------------------------------
                Module  : ifzone.h
                Version : 1.17
                Date    : 02/19/01
                Authors : Marius & Jianmin
----------------------------------------------------------------------
*/

#ifndef IFZONE_H
#define IFZONE_H

#include <stdio.h>

/*
-----------------------------------------------------------------------
Bound Definition
-----------------------------------------------------------------------
*/

#define IF_SIGN_LE \
  (1)

#define IF_SIGN_LT \
  (0)


typedef int if_type_bound;

#define if_make_bound(value,sign) \
  (((value) << 1) | (sign))

#define if_value_bound(bound) \
  ((bound) >> 1)

#define if_sign_bound(bound) \
  ((bound) & 1)

#define if_complement_bound(bound) \
  if_make_bound(-if_value_bound((bound)),!if_sign_bound((bound)))

#define if_min_bound(bound1,bound2) \
  ((bound1) < (bound2) ? (bound1) : (bound2))

#define if_max_bound(bound1,bound2) \
  ((bound1) < (bound2) ? (bound2) : (bound1))

#define if_add_bound(bound1,bound2) \
  ((((bound1) & ~1) + ((bound2) & ~1)) | ((bound1) & (bound2) & 1))

void if_print_bound(if_type_bound, FILE*);

#define IF_BOUND_INFTY \
  (0x10000000)




/*
-----------------------------------------------------------------------
Vector / Vector Table Definition
-----------------------------------------------------------------------
*/

typedef if_type_bound* if_type_vector;

typedef struct if_struct_vector_list {
  if_type_vector vector;
  struct if_struct_vector_list * next;
} * if_type_vector_list;

/*  *  */

if_type_vector if_create_vector(unsigned);

if_type_vector if_duplicate_vector(if_type_vector, unsigned);

void if_delete_vector(if_type_vector, unsigned);

void if_copy_vector(if_type_vector, if_type_vector, unsigned);

int if_compare_vector(if_type_vector, if_type_vector, unsigned);

unsigned if_hash_vector(if_type_vector, unsigned, unsigned);

void if_print_vector(if_type_vector, unsigned, FILE*);

/*  *  */

void if_init_vector_table();

void if_exit_vector_table();

void if_info_vector_table(FILE*);

if_type_vector if_put_vector(if_type_vector, unsigned);




/*
-----------------------------------------------------------------------
Matrix / Matrix Table Definition
-----------------------------------------------------------------------
*/

typedef if_type_vector* if_type_matrix;

typedef struct if_struct_matrix_list {
  if_type_matrix matrix;
  struct if_struct_matrix_list * next;
} * if_type_matrix_list;

/*  *  */

if_type_matrix if_create_matrix(unsigned);

void if_delete_matrix(if_type_matrix, unsigned);

void if_copy_matrix(if_type_matrix, if_type_matrix, unsigned);

int if_compare_matrix(if_type_matrix, if_type_matrix, unsigned);

unsigned if_hash_matrix(if_type_matrix, unsigned, unsigned);

void if_print_matrix(if_type_matrix, unsigned, FILE*);

/*  *  */

void if_init_matrix_table();

void if_exit_matrix_table();

void if_info_matrix_table(FILE*);

if_type_matrix if_put_matrix(if_type_matrix, unsigned);




/*
-----------------------------------------------------------------------
Zone Definition
-----------------------------------------------------------------------
*/

typedef if_type_matrix if_type_zone;

typedef struct if_struct_zone_list {
  if_type_zone zone;
  struct if_struct_zone_list * next;
} * if_type_zone_list;


#define if_copy_zone(dst,src) \
  (dst)=(src)

#define if_compare_zone(zone1,zone2) \
  ((zone1)!=(zone2))


if_type_zone if_empty_zone();

if_type_zone if_universal_zone();



int if_is_empty_zone(if_type_zone);

int if_clock_in_zone(if_type_zone, int);


if_type_zone if_clkcmp_zone(int, int, int, int);

if_type_zone if_clklt_zone(int, int, int);

if_type_zone if_clkle_zone(int, int, int);

if_type_zone if_clkge_zone(int, int, int);

if_type_zone if_clkgt_zone(int, int, int);

if_type_zone if_clkeq_zone(int, int, int);


int if_normalize_zone(if_type_zone);            /* internal use only */

int if_extrapolate_zone(if_type_zone);          /* internal use only */


if_type_zone if_intersect_zone(if_type_zone, if_type_zone);

if_type_zone if_exists_zone(if_type_zone, int);

if_type_zone if_set_zone(if_type_zone, int, int);

if_type_zone if_set_zone(if_type_zone, int, int, int);


if_type_zone if_round_zone(if_type_zone, int, int); 

if_type_zone if_open_right_zone(if_type_zone);

if_type_zone if_close_right_zone(if_type_zone);

if_type_zone if_open_left_zone(if_type_zone);

if_type_zone if_close_left_zone(if_type_zone);

if_type_zone if_open_zone(if_type_zone);

if_type_zone if_close_zone(if_type_zone);


if_type_zone if_backward_zone(if_type_zone);

if_type_zone if_forward_zone(if_type_zone);


if_type_zone if_since_zone(if_type_zone, if_type_zone);

if_type_zone if_until_zone(if_type_zone, if_type_zone);

if_type_zone if_diamond_zone(if_type_zone, int);


if_type_zone if_convex_hull_zone(if_type_zone, if_type_zone);


int if_include_zone(if_type_zone, if_type_zone);


void if_print_zone(if_type_zone, FILE*);

unsigned if_hash_zone(if_type_zone, unsigned);



/*
-----------------------------------------------------------------------
Zone Extrapolation
-----------------------------------------------------------------------
*/

void if_restrict_extrapolation_zone(if_type_zone);

void if_restrict_extrapolation_zone(int);



/*
-----------------------------------------------------------------------
Zone List
-----------------------------------------------------------------------
*/

void if_init_zone_list(if_type_zone_list*, if_type_zone);

void if_insert_zone_list(if_type_zone_list*, if_type_zone);

void if_clear_zone_list(if_type_zone_list*);


void if_intersect_zone_list(if_type_zone_list*, if_type_zone);

void if_intersect_zone_list(if_type_zone_list*, if_type_zone_list);


void if_guard_tpc_zone_list(if_type_zone_list*, if_type_zone, int);

void if_guard_split_zone_list(if_type_zone_list*, if_type_zone);

/*
-----------------------------------------------------------------------
Init / Exit Zone Module
-----------------------------------------------------------------------
*/

void if_init_zone_module();

void if_exit_zone_module();

#endif
