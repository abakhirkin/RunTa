#include "Util.hpp"
#include "Ta/Series.hpp"

#include "gtest/gtest.h"

#include <vector>

using namespace Ta;

TEST(SeriesTests, ToStringTest) {
    Series s0(SERIES_EMPTY);
    ASSERT_EQ("empty", U::toString(s0));

    Series s1(0);
    ASSERT_EQ("[0,0] -> 0", U::toString(s1));

    Series s2(0);
    s2.add(1, 0);
    s2.add(2, 0);
    s2.add(4, 1);
    s2.add(5, 1);
    ASSERT_EQ("[0,2] -> 0; [2,5] -> 1", U::toString(s2));
}

TEST(SeriesTests, AddTest) {
    Series s0(SERIES_EMPTY);
    ASSERT_EQ("empty", U::toString(s0));

    s0.add(0, 0);
    ASSERT_EQ("[0,0] -> 0", U::toString(s0));

    s0.add(0, 0);
    ASSERT_EQ("[0,0] -> 0", U::toString(s0));

    s0.add(1, 0);
    ASSERT_EQ("[0,1] -> 0", U::toString(s0));

    s0.add(1, 0);
    ASSERT_EQ("[0,1] -> 0", U::toString(s0));

    s0.add(2, 1);
    ASSERT_EQ("[0,1] -> 0; [1,2] -> 1", U::toString(s0));

    s0.add(2, 1);
    ASSERT_EQ("[0,1] -> 0; [1,2] -> 1", U::toString(s0));

    s0.add(3, 1);
    ASSERT_EQ("[0,1] -> 0; [1,3] -> 1", U::toString(s0));
}

TEST(SeriesTests, FindTimeTest) {
    Series s1(0);
    ASSERT_EQ(std::make_pair(false, (size_t)0), s1.findTime(-1));
    ASSERT_EQ(std::make_pair(false, (size_t)1), s1.findTime(0));
    ASSERT_EQ(std::make_pair(false, (size_t)1), s1.findTime(1));

    Series s2(0);    
    s2.add(2, 0);    
    s2.add(5, 1);
    ASSERT_EQ(std::make_pair(false, (size_t)0), s2.findTime(-1));
    ASSERT_EQ(std::make_pair(true, (size_t)1), s2.findTime(0));
    ASSERT_EQ(std::make_pair(true, (size_t)1), s2.findTime(1));
    ASSERT_EQ(std::make_pair(true, (size_t)2), s2.findTime(2));
    ASSERT_EQ(std::make_pair(true, (size_t)2), s2.findTime(3));
    ASSERT_EQ(std::make_pair(false, (size_t)3), s2.findTime(5));
    ASSERT_EQ(std::make_pair(false, (size_t)3), s2.findTime(6));

    Series s3(0);
    s3.add(2, 0);
    s3.add(5, 1);
    s3.add(6, 0);
    ASSERT_EQ(std::make_pair(false, (size_t)0), s3.findTime(-1));
    ASSERT_EQ(std::make_pair(true, (size_t)1), s3.findTime(0));
    ASSERT_EQ(std::make_pair(true, (size_t)1), s3.findTime(1));
    ASSERT_EQ(std::make_pair(true, (size_t)2), s3.findTime(2));
    ASSERT_EQ(std::make_pair(true, (size_t)2), s3.findTime(3));
    ASSERT_EQ(std::make_pair(true, (size_t)3), s3.findTime(5));
    ASSERT_EQ(std::make_pair(false, (size_t)4), s3.findTime(6));
    ASSERT_EQ(std::make_pair(false, (size_t)4), s3.findTime(7));

    Series s4(0);
    s4.add(2, 0);
    s4.add(5, 1);
    s4.add(6, 0);
    s4.add(8, 1);
    ASSERT_EQ(std::make_pair(false, (size_t)0), s4.findTime(-1));
    ASSERT_EQ(std::make_pair(true, (size_t)1), s4.findTime(0));
    ASSERT_EQ(std::make_pair(true, (size_t)1), s4.findTime(1));
    ASSERT_EQ(std::make_pair(true, (size_t)2), s4.findTime(2));
    ASSERT_EQ(std::make_pair(true, (size_t)2), s4.findTime(3));
    ASSERT_EQ(std::make_pair(true, (size_t)3), s4.findTime(5));
    ASSERT_EQ(std::make_pair(true, (size_t)4), s4.findTime(6));
    ASSERT_EQ(std::make_pair(true, (size_t)4), s4.findTime(7));
    ASSERT_EQ(std::make_pair(false, (size_t)5), s4.findTime(8));
    ASSERT_EQ(std::make_pair(false, (size_t)5), s4.findTime(9));
}

TEST(SeriesTests, FindTrueIntervalsTest) {
    Series s1(0);        
    std::vector<SegmentInterval> result1 = U::jItToVector<SegmentInterval, TrueIntervalsIt>(s1.findTrueIntervals(0, 1));
    ASSERT_EQ(std::vector<SegmentInterval> {}, result1);

    Series s2(0);
    s2.add(3, 1);
    std::vector<SegmentInterval> result20 = U::jItToVector<SegmentInterval, TrueIntervalsIt>(s2.findTrueIntervals(0, 0));
    ASSERT_EQ(std::vector<SegmentInterval> {SegmentInterval(0, 0, 3)}, result20);
    std::vector<SegmentInterval> result21 = U::jItToVector<SegmentInterval, TrueIntervalsIt>(s2.findTrueIntervals(1, 1));
    ASSERT_EQ(std::vector<SegmentInterval> {SegmentInterval(1, 1, 3)}, result21);
    std::vector<SegmentInterval> result22 = U::jItToVector<SegmentInterval, TrueIntervalsIt>(s2.findTrueIntervals(0, 3));
    ASSERT_EQ(std::vector<SegmentInterval> {SegmentInterval(0, 3, 3)}, result22);

    Series s3(0);
    s3.add(3, 0);
    std::vector<SegmentInterval> result31 = U::jItToVector<SegmentInterval, TrueIntervalsIt>(s3.findTrueIntervals(1, 1));
    ASSERT_EQ(std::vector<SegmentInterval> {}, result31);
    std::vector<SegmentInterval> result32 = U::jItToVector<SegmentInterval, TrueIntervalsIt>(s3.findTrueIntervals(0, 3));
    ASSERT_EQ(std::vector<SegmentInterval> {}, result32);

    Series s4(0);
    s4.add(2, 1);
    s4.add(4, 0);
    s4.add(6, 1);
    s4.add(7, 0);
    s4.add(8, 1);
    std::vector<SegmentInterval> result40 = U::jItToVector<SegmentInterval, TrueIntervalsIt>(s4.findTrueIntervals(0, 0));
    ASSERT_EQ(std::vector<SegmentInterval> {SegmentInterval(0, 0, 2)}, result40);
    std::vector<SegmentInterval> result41 = U::jItToVector<SegmentInterval, TrueIntervalsIt>(s4.findTrueIntervals(1, 1));
    ASSERT_EQ(std::vector<SegmentInterval> {SegmentInterval(1, 1, 2)}, result41);
    std::vector<SegmentInterval> result42 = U::jItToVector<SegmentInterval, TrueIntervalsIt>(s4.findTrueIntervals(1, 4));
    std::vector<SegmentInterval> expected42{ SegmentInterval(1, 2, 2), SegmentInterval(4, 4, 6) };
    ASSERT_EQ(expected42, result42);
    std::vector<SegmentInterval> result43 = U::jItToVector<SegmentInterval, TrueIntervalsIt>(s4.findTrueIntervals(2, 6));
    ASSERT_EQ(std::vector<SegmentInterval> {SegmentInterval(4, 6, 6)}, result43);
    std::vector<SegmentInterval> result44 = U::jItToVector<SegmentInterval, TrueIntervalsIt>(s4.findTrueIntervals(0, 8));
    std::vector<SegmentInterval> expected44{ SegmentInterval(0, 2, 2), SegmentInterval(4, 6, 6), SegmentInterval(7, 8, 8) };
    ASSERT_EQ(expected44, result44);
    std::vector<SegmentInterval> result45 = U::jItToVector<SegmentInterval, TrueIntervalsIt>(s4.findTrueIntervals(2, 2));
    ASSERT_EQ(std::vector<SegmentInterval> {}, result45);
}
