#pragma once

#include <string>
#include <vector>
#include <algorithm>

namespace U {

    template<typename T>
    std::string toString(const T &v) {
        std::stringstream ss;
        ss << v;
        return ss.str();
    }

    template<typename T, typename TIt>
    std::vector<T> jItToVector(TIt &it) {
        std::vector<T> result;
        while (it.hasNext())
            result.push_back(it.moveNext());
        return result;
    }

    template<typename TIt1, typename TIt2>
    bool setEqualOneSided(TIt1 begin1, TIt1 end1, TIt2 begin2, TIt2 end2) {
        for (TIt1 it1 = begin1; it1 != end1; ++it1) {
            bool found = false;
            for (TIt2 it2 = begin2; it2 != end2; ++it2) {
                if (*it1 == *it2) {
                    found = true;
                    break;
                }
            }
            if (!found)
                return false;
        }
        return true;
    }

    template<typename TIt1, typename TIt2>
    bool setEqual(TIt1 begin1, TIt1 end1, TIt2 begin2, TIt2 end2) {
        return setEqualOneSided(begin1, end1, begin2, end2) && setEqualOneSided(begin2, end2, begin1, end1);
    }

    template<typename TCol1, typename TCol2>
    bool setEqual(const TCol1 &col1, const TCol2 &col2) {
        return setEqual(col1.begin(), col1.end(), col2.begin(), col2.end());
    }
}