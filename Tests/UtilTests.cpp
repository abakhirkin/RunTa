#include "Util.hpp"

#include "gtest/gtest.h"

TEST(UtilTests, TestSetEqual) {
    std::vector<int> v0{};
    std::vector<int> v0_copy{};
    std::vector<int> v1 { 1, 2, 3 };
    std::vector<int> v2 { 3, 1, 2, 2 };
    std::vector<int> v3{ 4, 5, 3, 1, 2, 2 };
    std::vector<int> v4{ 1, 2, 3, 4, 5 };

    ASSERT_TRUE(U::setEqual(v0, v0_copy));
    ASSERT_FALSE(U::setEqual(v0, v1));
    ASSERT_FALSE(U::setEqual(v1, v0));
    ASSERT_TRUE(U::setEqual(v1, v1));
    ASSERT_TRUE(U::setEqual(v1, v2));
    ASSERT_TRUE(U::setEqual(v2, v1));
    ASSERT_FALSE(U::setEqual(v2, v3));
    ASSERT_FALSE(U::setEqual(v3, v2));
    ASSERT_TRUE(U::setEqual(v3, v4));
    ASSERT_TRUE(U::setEqual(v4, v3));
}

