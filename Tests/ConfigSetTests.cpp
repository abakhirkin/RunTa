#include "Util.hpp"
#include "Ta/ConfigSet.hpp"

#include "gtest/gtest.h"

using namespace Ta;

TEST(ConfigSetTests, StateConfigSetInsert) {
    State s(0);    
    Config c1(1, s, Zone{
        Constr(Clock(0), Clock(), Cmp::LEQ, 2), Constr(Clock(), Clock(0), Cmp::LEQ, -1),
        Constr(Clock(1), Clock(), Cmp::LEQ, 2), Constr(Clock(), Clock(1), Cmp::LEQ, -1),
    });    
    Config c3(3, s, Zone{
        Constr(Clock(0), Clock(), Cmp::LEQ, 1), Constr(Clock(), Clock(0), Cmp::LEQ, 0),
        Constr(Clock(1), Clock(), Cmp::LEQ, 3), Constr(Clock(), Clock(1), Cmp::LEQ, 0),
    });    
    Config c5(5, s, Zone{
        Constr(Clock(0), Clock(), Cmp::LEQ, 3), Constr(Clock(), Clock(0), Cmp::LEQ, 0),
        Constr(Clock(1), Clock(), Cmp::LEQ, 1), Constr(Clock(), Clock(1), Cmp::LEQ, 0),
    });
	Config c1Copy(1, s, Zone{
		Constr(Clock(0), Clock(), Cmp::LEQ, 2), Constr(Clock(), Clock(0), Cmp::LEQ, -1),
		Constr(Clock(1), Clock(), Cmp::LEQ, 2), Constr(Clock(), Clock(1), Cmp::LEQ, -1),
	});
	Config c3Copy(3, s, Zone{
		Constr(Clock(0), Clock(), Cmp::LEQ, 1), Constr(Clock(), Clock(0), Cmp::LEQ, 0),
		Constr(Clock(1), Clock(), Cmp::LEQ, 3), Constr(Clock(), Clock(1), Cmp::LEQ, 0),
	});
	Config c5Copy(5, s, Zone{
		Constr(Clock(0), Clock(), Cmp::LEQ, 3), Constr(Clock(), Clock(0), Cmp::LEQ, 0),
		Constr(Clock(1), Clock(), Cmp::LEQ, 1), Constr(Clock(), Clock(1), Cmp::LEQ, 0),
	});    

    StateConfigSet set;
	set.insert(c1);
	set.insert(c3);
	set.insert(c3Copy);
	set.insert(c5);
	set.insert(c1Copy);
	set.insert(c5Copy);
	
	std::vector<Config*> expected{ &c1, &c3, &c5 };
    std::vector<Config*> result(set.begin(), set.end());
    ASSERT_TRUE(U::setEqual(expected, result));    

	ASSERT_FALSE(c1.covered());
	ASSERT_FALSE(c3.covered());
	ASSERT_FALSE(c5.covered());

	ASSERT_TRUE(c1Copy.covered());
	ASSERT_EQ(&c1, c1Copy.coveredBy());
	ASSERT_TRUE(c3Copy.covered());
	ASSERT_EQ(&c3, c3Copy.coveredBy());
	ASSERT_TRUE(c5Copy.covered());
	ASSERT_EQ(&c5, c5Copy.coveredBy());
}