#include "Ta/Runner.hpp"
#include "Ta/MutAutomaton.hpp"
#include "Ta/SignalBuilder.hpp"

#include "Util.hpp"

#include "gtest/gtest.h"

using namespace Ta;

struct AcceptStateZone {
    const State &state;
    Zone zone;

    template<typename... TArgs>
    AcceptStateZone(const State &state, TArgs&&... zoneArgs) : state(state), zone(std::forward<TArgs>(zoneArgs)...) {}

    AcceptStateZone(const AcceptConfig &config) : state(config.parent().state()), zone(config.zone()) {}

    bool operator==(const AcceptStateZone &other) const {
        return state.id() == other.state.id() && zone == other.zone;
    }

    bool operator!=(const AcceptStateZone &other) const {
        return state.id() != other.state.id() || zone != other.zone;
    }
};

TEST(RunnerTests, Test1) {
    AtomRef b0(0), b1(1);
    PropSet propSet;

    SignalBuilder builder1(0, 6, 2);
    Series &series10 = builder1[b0];
    series10.add(3, 1);
    series10.add(6, 0);
    Series &series11 = builder1[b1];
    series11.add(1, 0);
    series11.add(6, 1);

    Signal signal1(builder1.build());
    PropSignal propSignal1(signal1, propSet);

    SignalBuilder builder2(6, 8, 2);
    Series &series20 = builder2[b0];
    series20.add(8, 1);
    Series &series21 = builder2[b1];
    series21.add(8, 1);

    Signal signal2(builder2.build());
    PropSignal propSignal2(signal2, propSet);

    MutAutomaton mutA;
    MutState &s0 = mutA.makeState();
    MutState &s1 = mutA.makeState();
    s0._inputInv = PropRef(b0);
    mutA.makeInit(s0);
    s1._inputInv = PropRef(b1);
    s1._acceptCond = Zone(ZONE_TOP);
    mutA.makeTrans(s0, s1);

    Automaton a = mutA.build();

    Runner r(a, Clock(0));
    r.run(propSignal1);

    std::vector<AcceptStateZone> actual1;
    for (size_t i = 0, count = r.acceptConfigCount(); i < count; ++i)
        actual1.emplace_back(r.acceptConfig(i));
    std::vector<AcceptStateZone> expected1;
    expected1.emplace_back(a.state(1), Zone { Constr(Clock(0), Clock(), Cmp::LEQ, 6), Constr(Clock(), Clock(0), Cmp::LEQ, -1) });
    ASSERT_EQ(expected1, actual1);

    r.run(propSignal2);

   std::vector<AcceptStateZone> actual2;
    for (size_t i = 0, count = r.acceptConfigCount(); i < count; ++i)
        actual2.emplace_back(r.acceptConfig(i));
    std::vector<AcceptStateZone> expected2;    
    expected2.emplace_back(a.state(1), Zone{ Constr(Clock(0), Clock(), Cmp::LEQ, 8), Constr(Clock(), Clock(0), Cmp::LEQ, -6) });
    ASSERT_EQ(expected2, actual2);
}

TEST(RunnerTests, Test2) {
    AtomRef b0(0), b1(1);
    PropSet propSet;

    SignalBuilder builder1(0, 4, 2);
    Series &series10 = builder1[b0];
    series10.add(1, 0);
    series10.add(3, 1);
    series10.add(4, 0);
    Series &series11 = builder1[b1];
    series11.add(4, 1);
    Signal signal1(builder1.build());
    PropSignal propSignal1(signal1, propSet);

    SignalBuilder builder2(4, 9, 2);
    Series &series20 = builder2[b0];
    series20.add(6, 1);
    series20.add(9, 0);
    Series &series21 = builder2[b1];
    series21.add(5, 1);
    series21.add(6, 0);
    series21.add(7, 1);
    series21.add(8, 0);
    series21.add(9, 1);
    Signal signal2(builder2.build());
    PropSignal propSignal2(signal2, propSet);

    MutAutomaton mutA;
    Clock clock(0);
    Clock time(1);
    MutState &s0 = mutA.makeState();
    s0._inputInv = PropRef(PROP_TRUE);
    mutA.makeInit(s0);
    MutState &s1 = mutA.makeState();
    s1._inputInv = PropRef(b0);
    MutTrans &t01 = mutA.makeTrans(s0, s1);
    t01._commands = CmdList(Cmd(CMD_RESET, clock));
    MutState &s2 = mutA.makeState();
    s2._inputInv = PropRef(b1);
    s2._acceptCond = Zone{ Constr(clock, Clock(), Cmp::LEQ, 3) };
    mutA.makeTrans(s1, s2);

    Automaton a = mutA.build();

    Runner r(a, time);
    r.run(propSignal1);

    std::vector<AcceptStateZone> actual1;
    for (size_t i = 0, count = r.acceptConfigCount(); i < count; ++i)
        actual1.emplace_back(r.acceptConfig(i));
    std::vector<AcceptStateZone> expected1;
    expected1.emplace_back(a.state(2), Zone {
        Constr(Clock(0), Clock(), Cmp::LEQ, 3), Constr(Clock(), Clock(0), Cmp::LEQ, 0),
        Constr(Clock(1), Clock(), Cmp::LEQ, 4), Constr(Clock(), Clock(1), Cmp::LEQ, -1),
        Constr(Clock(1), Clock(0), Cmp::LEQ, 3), Constr(Clock(0), Clock(1), Cmp::LEQ, -1)
    });
    ASSERT_EQ(expected1, actual1);

    r.run(propSignal2);
    std::vector<AcceptStateZone> actual2;
    for (size_t i = 0, count = r.acceptConfigCount(); i < count; ++i)
        actual2.emplace_back(r.acceptConfig(i));
    std::vector<AcceptStateZone> expected2;    
    expected2.emplace_back(a.state(2), Zone{
        Constr(Clock(0), Clock(), Cmp::LEQ, 1), Constr(Clock(), Clock(0), Cmp::LEQ, 0),
        Constr(Clock(1), Clock(), Cmp::LEQ, 5), Constr(Clock(), Clock(1), Cmp::LEQ, -4),
        Constr(Clock(1), Clock(0), Cmp::LEQ, 5), Constr(Clock(0), Clock(1), Cmp::LEQ, -4)
    });
    expected2.emplace_back(a.state(2), Zone{
        Constr(Clock(0), Clock(), Cmp::LEQ, 3), Constr(Clock(), Clock(0), Cmp::LEQ, 0),
        Constr(Clock(1), Clock(), Cmp::LEQ, 5), Constr(Clock(), Clock(1), Cmp::LEQ, -4),
        Constr(Clock(1), Clock(0), Cmp::LEQ, 3), Constr(Clock(0), Clock(1), Cmp::LEQ, -1)
    });
    expected2.emplace_back(a.state(2), Zone{
        Constr(Clock(0), Clock(), Cmp::LEQ, 3), Constr(Clock(), Clock(0), Cmp::LEQ, 0),
        Constr(Clock(1), Clock(), Cmp::LEQ, 7), Constr(Clock(), Clock(1), Cmp::LEQ, -6),
        Constr(Clock(1), Clock(0), Cmp::LEQ, 6), Constr(Clock(0), Clock(1), Cmp::LEQ, -4)
    });
    ASSERT_TRUE(U::setEqual(expected2, actual2));
}

TEST(RunnerTests, Test3) {
    AtomRef b0(0), b1(1), b2(2), b3(3), b4(4);
    PropSet propSet;

    SignalBuilder builder(0, 4, 5);
    Series &series0 = builder[b0];
    series0.add(4, 1);
    Series &series1 = builder[b1];
    series1.add(4, 1);
    Series &series2 = builder[b2];
    series2.add(4, 1);
    Series &series3 = builder[b3];
    series3.add(4, 1);
    Series &series4 = builder[b4];
    series4.add(4, 1);
    Signal signal(builder.build());
    PropSignal propSignal(signal, propSet);

    MutAutomaton mutA;
    Clock time(0);
    Clock clock(1);

    MutState &s0 = mutA.makeState();
    s0._inputInv = PropRef(b0);

    MutState &s1 = mutA.makeState();
    s1._inputInv = PropRef(b1);
    s1._allowTime = false;

    MutState &s2 = mutA.makeState();
    s2._inputInv = PropRef(b2);
    s2._allowTime = false;

    MutState &s3 = mutA.makeState();
    s3._inputInv = PropRef(b3);

    MutState &s4 = mutA.makeState();
    s4._inputInv = PropRef(b4);
    s4._acceptCond = Zone(ZONE_TOP);

    mutA.makeInit(s0, Cmd(CMD_RESET, clock));
    mutA.makeTrans(s0, s1);
    mutA.makeTrans(s1, s2);
    mutA.makeTrans(s2, s3);
    MutTrans &t03 = mutA.makeTrans(s0, s3);
    t03._guard = Zone{ Constr(Clock(), clock, Cmp::LEQ, -1) };
    mutA.makeTrans(s3, s4);

    Automaton a = mutA.build();

    Runner r(a, time);
    r.run(propSignal);
    std::cout << toDot(r) << "\n";

    // TODO: Come up with something to test here.
}

TEST(RunnerTests, TestContinueFromPoint) {
	// Tests continuation from a symbolic state that cannot be time-elapsed.
	// There used to be a bug in it.

	PropSet propSet;

	AtomRef b0(0), b1(1);
	PropRef b0NotB1 = propSet.makeAnd({ PropRef(b0), propSet.makeNot(PropRef(b1)) });
	PropRef b0B1 = propSet.makeAnd({ PropRef(b0), PropRef(b1) });
	PropRef notB0B1 = propSet.makeAnd({ propSet.makeNot(PropRef(b0)), PropRef(b1) });

	Clock clockMatch(0);
	Clock clockB0(1);
	Clock clockB0B1(2);
	Clock clockB1(3);

	MutAutomaton mutA;

	MutState &s0 = mutA.makeState();
	mutA.makeInit(s0);

	MutState &s1 = mutA.makeState();
	s1._inputInv = b0NotB1;
	s1._clockInv = Zone{ Constr(clockB0, Clock(), Cmp::LEQ, 5) };

	MutTrans &t01 = mutA.makeTrans(s0, s1);
	t01._commands = CmdList{ Cmd(CMD_RESET, clockMatch), Cmd(CMD_RESET, clockB0) };

	MutState &s2 = mutA.makeState();
	s2._inputInv = b0B1;
	s2._clockInv = Zone{
		Constr(clockB0, Clock(), Cmp::LEQ, 5),
		Constr(clockB0B1, Clock(), Cmp::LEQ, 2),
		Constr(clockB1, Clock(), Cmp::LEQ, 5) };

	MutTrans &t12 = mutA.makeTrans(s1, s2);
	t12._commands = CmdList({ Cmd(CMD_RESET, clockB0B1), Cmd(CMD_RESET, clockB1) });

	MutState &s3 = mutA.makeState();
	s3._inputInv = notB0B1;
	s3._clockInv = Zone{ Constr(clockB1, Clock(), Cmp::LEQ, 5) };
	s3._acceptCond = Zone{ Constr(clockB1, Clock(), Cmp::LEQ, 5), Constr(Clock(), clockB1, Cmp::LEQ, -4), };

	MutTrans &t23 = mutA.makeTrans(s2, s3);
	t23._guard = Zone{
		Constr(clockB0, Clock(), Cmp::LEQ, 5), Constr(Clock(), clockB0, Cmp::LEQ, -4),
		Constr(clockB0B1, Clock(), Cmp::LEQ, 2), Constr(Clock(), clockB0B1, Cmp::LEQ, -1)
	};

	Automaton a(mutA.build());

	SignalBuilder builder1(0, 30, 2);
	Series &series10 = builder1[b0];	
	series10.add(10, 0);
	series10.add(15, 1);
	series10.add(25, 0);
	series10.add(30, 1);
	Series &series11 = builder1[b1];
	series11.add(28, 0);
	series11.add(30, 1);
	Signal signal1(builder1.build());

	// At time 30, the automaton can continue from state s2.
	// This state cannot be time-elapsed, but there can be a transition to s3.

	SignalBuilder builder2(30, 60, 2);
	Series &series20 = builder2[b0];
	series20.add(40, 0);
	series20.add(45, 1);
	series20.add(55, 0);
	series20.add(60, 1);
	Series &series21 = builder2[b1];
	series21.add(32, 1);
	series21.add(60, 0);	
	Signal signal2(builder2.build());

	PropSignal propSignal1(signal1, propSet);
	PropSignal propSignal2(signal2, propSet);

	Clock time(99);
	Runner r(a, time);
	r.run(propSignal1);
	r.run(propSignal2);

	ASSERT_EQ(r.acceptConfigCount(), 1);
	const Zone &acceptZone = r.acceptConfig(0).zone();
	ASSERT_EQ(Bound(Cmp::LEQ, 32), acceptZone.bound(time, Clock()));
	ASSERT_EQ(Bound(Cmp::LEQ, -32), acceptZone.bound(Clock(), time));
}

TEST(RunnerTests, TestRaisingEdge) {

	PropSet propSet;

	AtomRef b0(0), b1(1);
	PropRef notB0 = propSet.makeNot(PropRef(b0));
	PropRef notB1 = propSet.makeNot(PropRef(b1));

	MutAutomaton mutA;

	MutState &s0 = mutA.makeState();
	s0._inputInv = notB0;
	mutA.makeInit(s0);

	MutState &s1 = mutA.makeState();
	s1._inputInv = notB1;
	s1._allowTime = false;
	mutA.makeTrans(s0, s1);

	MutState &s2 = mutA.makeState();
	s2._inputInv = PropRef(b1);
	s2._allowTime = false;
	mutA.makeTrans(s1, s2);

	MutState &s3 = mutA.makeState();
	s3._inputInv = PropRef(b0);
	s3._acceptCond = Zone(ZONE_TOP);
	mutA.makeTrans(s2, s3);

	Automaton automaton(mutA.build());

	SignalBuilder builder(0, 5, 2);
	Series &series0 = builder[b0];
	series0.add(2, 0);
	series0.add(5, 1);
	Series &series1 = builder[b1];
	series1.add(2, 0);
	series1.add(3, 1);
	series1.add(5, 0);
	Signal signal(builder.build());
	PropSignal propSignal(signal, propSet);

	Clock time(99);
	Runner r(automaton, time);
	r.run(propSignal);
	
	ASSERT_EQ(r.acceptConfigCount(), 1);
	const Zone &acceptZone = r.acceptConfig(0).zone();
	ASSERT_EQ(Bound(Cmp::LEQ, 5), acceptZone.bound(time, Clock()));
	ASSERT_EQ(Bound(Cmp::LEQ, -2), acceptZone.bound(Clock(), time));	
}
