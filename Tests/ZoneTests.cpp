#include "Util.hpp"
#include "Ta/Zone.hpp"

#include "gtest/gtest.h"

using namespace Ta;

TEST(ZoneTests, BoundTest) {
    Zone z1 {
        Constr(Clock(0), Clock(), Cmp::LEQ, 1), Constr(Clock(), Clock(0), Cmp::LEQ, 0),
        Constr(Clock(1), Clock(), Cmp::LEQ, 1), Constr(Clock(), Clock(1), Cmp::LEQ, 0) };

    ASSERT_EQ(Bound(Cmp::LEQ, 1), z1.bound(Clock(0), Clock()));
    ASSERT_EQ(Bound(Cmp::LEQ, 0), z1.bound(Clock(), Clock(0)));
    ASSERT_EQ(Bound(Cmp::LEQ, 1), z1.bound(Clock(1), Clock()));
    ASSERT_EQ(Bound(Cmp::LEQ, 0), z1.bound(Clock(), Clock(1)));
    ASSERT_EQ(Bound(Cmp::LEQ, 1), z1.bound(Clock(0), Clock(1)));
    ASSERT_EQ(Bound(Cmp::LEQ, 1), z1.bound(Clock(1), Clock(0)));
}

TEST(ZoneTests, BoundDisjointTest) {
    Bound b1(Cmp::LEQ, 1);
    Bound b2(Cmp::LEQ, -2);    
    ASSERT_TRUE(b1.isNegDisjoint(b2));

    Bound b3(Cmp::LT, 1);
    ASSERT_FALSE(b1.isNegDisjoint(b3));

    Bound b4(Cmp::LEQ, -1);
    ASSERT_FALSE(b1.isNegDisjoint(b4));

    Bound b5(Cmp::LT, -1);
    ASSERT_TRUE(b1.isNegDisjoint(b5));
}

