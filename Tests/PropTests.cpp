#include "Util.hpp"
#include "Ta/PropSet.hpp"

#include "gtest/gtest.h"

using namespace Ta;

TEST(PropTests, ToStringTest) {
    AtomRef b0(0), b1(1), b2(2);
    PropSet set;
    PropRef ptrue = set.makeTrue();
    PropRef pfalse = set.makeFalse();
    PropRef p0 = set.makeAtom(b0);
    PropRef p1 = set.makeAtom(b1);
    PropRef p2 = set.makeAtom(b2);
    PropRef p3 = set.makeAnd({ p0, p1, p1, p0 });
    PropRef p4 = set.makeOr({ p3, p2, p3 });

    ASSERT_EQ("true", U::toString(ptrue));
    ASSERT_EQ("false", U::toString(pfalse));
    ASSERT_EQ("b0", U::toString(p0));
    ASSERT_EQ("b1", U::toString(p1));
    ASSERT_EQ("b2", U::toString(p2));
    ASSERT_EQ("(and b0 b1)", U::toString(set.expr(p3)));
    ASSERT_EQ("(or b2 p0)", U::toString(set.expr(p4)));
}

TEST(PropTests, HashTest) {
    AtomRef b0(0), b1(1), b2(2);
    PropSet set;
    PropRef ptrue = set.makeTrue();
    PropRef pfalse = set.makeFalse();
    PropRef p0 = set.makeAtom(b0);
    PropRef p1 = set.makeAtom(b1);
    PropRef p2 = set.makeAtom(b2);
    PropRef p3 = set.makeAnd({ p0, p1 });
    PropRef p4 = set.makeOr({ p3, p2 });

    PropRef ptrueCopy = set.makeTrue();
    PropRef pfalseCopy = set.makeFalse();
    PropRef p0Copy = set.makeAtom(b0);
    PropRef p1Copy = set.makeAtom(b1);
    PropRef p2Copy = set.makeAtom(b2);
    PropRef p3Copy = set.makeAnd({ p0Copy, p1Copy });
    PropRef p3Copy2 = set.makeAnd({ p1Copy, p0Copy, p1Copy });
    PropRef p4Copy = set.makeOr({ p3Copy, p2Copy });
    PropRef p4Copy2 = set.makeOr({ p2Copy, p2Copy, p3Copy, p3Copy });

    ASSERT_EQ(ptrue, ptrueCopy);
    ASSERT_EQ(pfalse, pfalseCopy);
    ASSERT_EQ(p0, p0Copy);
    ASSERT_EQ(p1, p1Copy);
    ASSERT_EQ(p2, p2Copy);
    ASSERT_EQ(p3, p3Copy);
    ASSERT_EQ(p3, p3Copy2);
    ASSERT_EQ(p4, p4Copy);
    ASSERT_EQ(p4, p4Copy2);
}
