#include "Util.hpp"
#include "Ta/Config.hpp"

#include "gtest/gtest.h"

using namespace Ta;

TEST(ConfigTests, ToStringTest) {
    State s(0);
    Config c(0, s, Zone{
        Constr(Clock(0), Clock(), Cmp::LEQ, 1), Constr(Clock(), Clock(0), Cmp::LEQ, 0),
        Constr(Clock(1), Clock(), Cmp::LEQ, 1), Constr(Clock(), Clock(1), Cmp::LEQ, 0),
    });
    ASSERT_EQ("0 @ s0\n0 <= c0 <= 1\n0 <= c1 <= 1\n-1 <= c1 - c0 <= 1", U::toString(c));
}