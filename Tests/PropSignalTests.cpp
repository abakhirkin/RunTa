#include "Util.hpp"
#include "Ta/PropSignal.hpp"
#include "Ta/SignalBuilder.hpp"

#include "gtest/gtest.h"

using namespace Ta;

TEST(SignalTests, PropSignalTest) {
    // This creates a signal [0, 6] with 2 series that are initialized to start at 0, but don't have other segments.
    SignalBuilder builder(0, 6, 2);
    AtomRef b0(0), b1(1);

    // This adds segments to series so that they correctly end at time 6.
    Series &s0 = builder[b0];
    s0.add(1, 1);
    s0.add(2, 0);
    s0.add(3, 1);
    s0.add(4, 0);
    s0.add(5, 1);
    s0.add(6, 0);
    Series &s1 = builder[b1];
    s1.add(2, 1);
    s1.add(4, 0);
    s1.add(6, 1);

    Signal signal(builder.build());

    PropSet pSet;
    PropRef ptrue = pSet.makeTrue();
    PropRef pfalse = pSet.makeFalse();
    PropRef p0 = pSet.makeAtom(b0);
    PropRef p1 = pSet.makeAtom(b1);
    PropRef p0and1 = pSet.makeAnd({ p0, p1 });
    PropRef p0or1 = pSet.makeOr({ p0, p1 });

    PropSignal propSignal(signal, pSet);

    ASSERT_EQ("[0,2] -> 1; [2,4] -> 0; [4,6] -> 1", U::toString(propSignal[p1]));	
	ASSERT_GT(p0or1.id(), p0and1.id());
	// NOTE: Here we Request the PropRef with higher id first. There used to be a bug in this case.
	ASSERT_EQ("[0,3] -> 1; [3,4] -> 0; [4,6] -> 1", U::toString(propSignal[p0or1]));
    ASSERT_EQ("[0,1] -> 1; [1,4] -> 0; [4,5] -> 1; [5,6] -> 0", U::toString(propSignal[p0and1]));    
    ASSERT_EQ("[0,6] -> 1", U::toString(propSignal[ptrue]));
    ASSERT_EQ("[0,6] -> 0", U::toString(propSignal[pfalse]));
}